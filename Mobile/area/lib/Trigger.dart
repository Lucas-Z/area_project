import 'package:flutter/material.dart';
import 'package:area/global.dart';
import 'package:requests/requests.dart';

class TriggerPage extends StatefulWidget{
  @override
  _TriggerPageState createState() => _TriggerPageState();
}

class _TriggerPageState extends State<TriggerPage>{

  List<List<String>> triggers = [];

  void getListofTrigger() async {
    List<List<String>> trigger = [];
    var url = "http://"+ ipserver +":8080/services/getTriggersDesc";
    var response = await Requests.get(url);
    response.raiseForStatus();
    for (var i = 0; i < response.json().length; i++) {
      trigger.add([response.json()[i][0].toString(), response.json()[i][1].toString()]);
    }
    if ((triggers.length == 0 && trigger.length != 0) || trigger.length != trigger.length) {
      setState(() {
        triggers = trigger;
      });
    }
  }

  void removeTrigger(int index) async {
    var url = "http://"+ ipserver +":8080/services/removeTrigger";
    await Requests.post(url, body: {
    "triggerIdx": index,
    },);
    getListofTrigger();
  }

  @override
  Widget build(BuildContext context) {
    getListofTrigger();
    return Scaffold(
      appBar: AppBar (
        backgroundColor: Colors.grey[800],
          centerTitle: true,
          title: Text("List of Trigger"),
      ),
      body: ListView.separated (
        separatorBuilder: (
          BuildContext context, int index) {
            return SizedBox(
              height: 30,
            );
          },
        padding: const EdgeInsets.all(20),
        itemCount: triggers.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onLongPress: () => removeTrigger(index),
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color : Colors.blueAccent,
                borderRadius: BorderRadius.all(
                    Radius.circular(50.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 10.0,
                    spreadRadius: 5.0,
                    offset: Offset(
                      5.0,
                      5.0,
                    ),
                  )
                ],
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(triggers[index][0],
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(width: 8),
                  Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  ),
                  SizedBox(width: 8),
                  Text(triggers[index][1],
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            ),
          );
        }
      ),
    );
  }
}