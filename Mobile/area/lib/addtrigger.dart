import 'package:flutter/material.dart';
import 'package:area/global.dart';
import 'package:requests/requests.dart';
import 'package:area/reactions.dart';

class AddTriggerPage extends StatefulWidget{
  @override
  _AddTriggerPageState createState() => _AddTriggerPageState();
}

class _AddTriggerPageState extends State<AddTriggerPage>{
  String action;
  Map<String, String> actionParams = {};
  String reaction;
  Map<String, String> reactionParams = {};

  void confGithubAction(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'Owner'
                ),
                onChanged: (text) {
                  result["owner"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Repository'
                ),
                onChanged: (text) {
                  result["repo"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                actionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confNothingAction(String value, context) {
    actionParams = {};
  }

  void confDiscordActionMemberChannel(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'Guild'
                ),
                onChanged: (text) {
                  result["guild"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                actionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confDiscordActionMessage(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'Guild'
                ),
                onChanged: (text) {
                  result["guild"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Channel'
                ),
                onChanged: (text) {
                  result["channel"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                actionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confOpenWeatherActionTemp(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'City'
                ),
                onChanged: (text) {
                  result["city"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                actionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confOpenWeatherActionForecast(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'City'
                ),
                onChanged: (text) {
                  result["city"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                actionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confDiscordReactionMessage(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'Guild'
                ),
                onChanged: (text) {
                  result["guild"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Channel'
                ),
                onChanged: (text) {
                  result["channel"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Message'
                ),
                onChanged: (text) {
                  result["message"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                reactionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confDiscordReactionRole(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              Text("Be careful that the bot has enough permission"),
              TextField(
                decoration: InputDecoration(
                  labelText: 'User'
                ),
                onChanged: (text) {
                  result["user"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Guild'
                ),
                onChanged: (text) {
                  result["guild"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Role'
                ),
                onChanged: (text) {
                  result["role"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                reactionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confGithubReactionMerge(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'Owner'
                ),
                onChanged: (text) {
                  result["owner"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Repo'
                ),
                onChanged: (text) {
                  result["repo"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Base'
                ),
                onChanged: (text) {
                  result["base"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Head'
                ),
                onChanged: (text) {
                  result["head"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Commit message'
                ),
                onChanged: (text) {
                  result["commit_message"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                reactionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confMailjetReactionSendmail(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'Email to'
                ),
                onChanged: (text) {
                  result["emailTo"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Name'
                ),
                onChanged: (text) {
                  result["name"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Subject'
                ),
                onChanged: (text) {
                  result["subject"] = text;
                },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Message'
                ),
                onChanged: (text) {
                  result["message"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                reactionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confNothingReaction(String value, context) {
    actionParams = {};
  }

  void confSpotifyReactionSkip(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'Skip'
                ),
                onChanged: (text) {
                  result["skip"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                reactionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  void confSpotifyReactionState(String value, context) {
    Map<String, String> result = {};
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'State'
                ),
                onChanged: (text) {
                  result["state"] = text;
                },
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                reactionParams = result;
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          DropdownButton<String> (
            items: <String>["new github commit", "new github clone", "new deezer follower", "new deezer recommendations", "new discord member", "new discord channel", "new discord message", "new spotify current track", "new spotify top artist", "new openweather temperature", "new openweather weather forecast"]
              .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: actionRoute[actionDesc.indexOf(value)],
                  child: Text(value, style: TextStyle(color: Colors.black)),
                );
              })
              .toList(),
            hint: Text('Choose an action', style: TextStyle(color: Colors.black)),
            onChanged: (String newValue) {
              if (newValue == "/services/deezer/action/followers" || newValue == "/services/deezer/action/recommendations" ||
                newValue == "/services/spotify/action/currentTrack" || newValue == "/services/spotify/action/topArtist")
                confNothingAction(newValue, context);
              else if (newValue == "/services/github/action/commit" || newValue == "/services/github/action/clone")
                confGithubAction(newValue, context);
              else if (newValue == "/services/discord/action/members" || newValue == "/services/discord/action/channels")
                confDiscordActionMemberChannel(newValue, context);
              else if (newValue == "/services/discord/action/channels/message")
                confDiscordActionMessage(newValue, context);
              else if (newValue == "/services/openweather/action/temp")
                confOpenWeatherActionTemp(newValue, context);
              else if (newValue == "/services/openweather/action/weatherforecast")
                confOpenWeatherActionForecast(newValue, context);
              setState(() {
                action = newValue;
              });
            },
            value: action,
          ),
          DropdownButton<String> (
            items: <String>["post new discord message", "add a role to a discord user", "merge a github repo", "send a mail", "clear all trello notifications", "skip current track on spotify", "change the player state on spotify"]
              .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: reactionRoute[reactionDesc.indexOf(value)],
                  child: Text(value, style: TextStyle(color: Colors.black)),
                );
              })
              .toList(),
            hint: Text('Choose a reaction', style: TextStyle(color: Colors.black)),
            onChanged: (String newValue) {
              if (newValue == "/services/trello/reaction/notif")
                confNothingReaction(newValue, context);
              else if (newValue == "/services/discord/reaction/message")
                confDiscordReactionMessage(newValue, context);
              else if (newValue == "/services/discord/reaction/role")
                confDiscordReactionRole(newValue, context);
              else if (newValue == "/services/github/reaction/merge")
                confGithubReactionMerge(newValue, context);
              else if (newValue == "/services/mailjet/reaction/sendmail")
                confMailjetReactionSendmail(newValue, context);
              else if (newValue == "/services/spotify/reaction/playback/skip")
                confSpotifyReactionSkip(newValue, context);
              else if (newValue == "/services/spotify/reaction/playback/state")
                confSpotifyReactionState(newValue, context);
              setState(() {
                reaction = newValue;
              });
            },
            value: reaction,
          ),
          FlatButton(
            child: Text("Add Trigger"),
            onPressed: () async {
              dynamic jsonTrigger = {
                "trigger": {
                  "action": {
                    "route": action,
                    "params": actionParams
                  },
                  "reaction": {
                    "route": reaction,
                    "params": reactionParams
                  },
                  "data": null
                }
              };
              await Requests.post(
                "http://" + ipserver + ":8080/services/addTrigger",
                bodyEncoding: RequestBodyEncoding.JSON,
                body: jsonTrigger
              );
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}