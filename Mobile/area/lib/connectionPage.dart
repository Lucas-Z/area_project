import 'package:flutter/material.dart';

class ConnectionPage extends StatefulWidget{
  @override
  _ConnectionPageState createState() => _ConnectionPageState();
}

class _ConnectionPageState extends State<ConnectionPage>{

  void _onPressedRegister() {
    Navigator.pushNamed(context, '/register');
  }

  void _onPressedLogin() {
    Navigator.pushNamed(context, '/login');
  }

  void _onPressedGoogle() {
    Navigator.pushNamed(context, '/auth/google');
  }

  void _onPressedGithub() {
    Navigator.pushNamed(context, '/auth/github');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "AREA",
                style: TextStyle(
                  fontSize: 50,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 50),
              Text(
                "Welcome !  Register to use AREA !",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 50),
              Container(
                child: FlatButton.icon(
                  padding: const EdgeInsets.all(5.0),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                  onPressed: _onPressedGoogle,
                  icon: Image.asset('assets/google_logo.png'),
                  label: Text('GOOGLE'),
                  color: Colors.white,
                ),
                height: 40,
                width: 300,
              ),
              SizedBox(height: 20),
              Container(
                child: FlatButton.icon(
                  padding: const EdgeInsets.all(5.0),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                  onPressed: _onPressedGithub,
                  icon: Image.asset('assets/logoGithub.png'),
                  label: Text(
                    'GITHUB',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.black
                ),
                height: 40,
                width: 300,
              ),
              SizedBox(height: 20),
              Container(
                child: FlatButton.icon(
                  padding: const EdgeInsets.all(5.0),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                  onPressed: _onPressedRegister,
                  icon: Icon(
                    Icons.mail,
                    color: Colors.white,
                  ),
                  label: Text(
                    'E-MAIL',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blueAccent,
                ),
                height: 40,
                width: 300,
              ),
              SizedBox(height: 20),
              Container(
                child: FlatButton(
                  onPressed: _onPressedLogin,
                  child: Text(
                    "Sign in ?",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                height: 40,
                width: 300,
              ),
            ],
          ),
        ),
      ),
    );
  }
}