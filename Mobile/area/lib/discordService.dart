import 'package:flutter/material.dart';

class DiscordPage extends StatefulWidget {

  @override
  _DiscordPageState createState() => _DiscordPageState();
}

class _DiscordPageState extends State<DiscordPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar (
        backgroundColor: Colors.white,
          centerTitle: true,
          title: Text("Home", style: TextStyle(color: Color.fromRGBO(121, 141, 207, 1)),),
          actions: <Widget> [
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset('assets/discord.png')
                )
              ],
            )
          ]
        ),
    );
  }
}