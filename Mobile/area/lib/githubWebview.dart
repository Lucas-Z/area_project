import 'package:flutter/material.dart';
import 'package:area/global.dart';
import 'package:requests/requests.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class GithubWebView extends StatelessWidget {

  InAppWebViewController webView;
  CookieManager manager = CookieManager();

  @override
  Widget build(BuildContext context) {
    manager.deleteAllCookies();
    return Scaffold(
      body: InAppWebView(
        initialOptions: InAppWebViewWidgetOptions(),
        initialUrl: "http://"+ ipserver +":8080/auth/github",
        onWebViewCreated: (InAppWebViewController controller) {
          webView = controller;
        },
        onLoadStop: (InAppWebViewController controller, String url) async {
          if ((url.indexOf('/services/github/redirect') != -1 && url.indexOf('state=login') != -1) || url.indexOf(ipserver + ":8081/") != -1) {
            var cookie = await manager.getCookie(url: "http://"+ ipserver+":8080/", name: "token");
            print(cookie.value.toString());
            String hostname = Requests.getHostname("http://"+ ipserver +":8080/");
            await Requests.setStoredCookies(hostname, {'token': cookie.value.toString()});
            Navigator.pushReplacementNamed(context, '/home');
          }
          print(url);
        }
      )
    );
  }
}