import 'package:flutter/material.dart';
import 'package:area/global.dart';
import 'package:requests/requests.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';


class GoogleWebView extends StatelessWidget {

  InAppWebViewController webView;
  CookieManager manager = CookieManager();

  @override
  Widget build(BuildContext context) {
    manager.deleteAllCookies();
    return Scaffold(
      body: InAppWebView(
        initialOptions: InAppWebViewWidgetOptions(
          inAppWebViewOptions: InAppWebViewOptions(
            userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0) Gecko/20100101 Firefox/61.0",
          )
        ),
        initialUrl: "http://"+ ipserver +":8080/auth/google",
        onWebViewCreated: (InAppWebViewController controller) {
          webView = controller;
        },
        onLoadStop: (InAppWebViewController controller, String url) async {
          print(url);
          if (url.indexOf('/auth/google/callback') != -1 || url.indexOf(ipserver + ":8081/") != -1) {
            var cookie = await manager.getCookie(url: "http://"+ ipserver+":8080/", name: "token");
            print(cookie.value.toString());
            String hostname = Requests.getHostname("http://"+ ipserver +":8080/");
            await Requests.setStoredCookies(hostname, {'token': cookie.value.toString()});
            Navigator.pushReplacementNamed(context, '/home');
          }
          print(url);
        }
      )
    );
  }
}