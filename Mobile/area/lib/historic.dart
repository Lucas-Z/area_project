import 'package:flutter/material.dart';
import 'package:area/global.dart';
import 'package:requests/requests.dart';

class HistoricPage extends StatefulWidget{
  @override
  _HistoricPageState createState() => _HistoricPageState();
}

class _HistoricPageState extends State<HistoricPage>{

  List<String> triggers = [];

  void getListofTrigger() async {
    if ((triggers.length == 0 && historic.length != 0) || triggers.length != historic.length) {
      setState(() {
        triggers = historic;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    getListofTrigger();
    return Scaffold(
      appBar: AppBar (
        backgroundColor: Colors.grey[800],
          centerTitle: true,
          title: Text("History of Trigger"),
      ),
      body: ListView.separated (
        separatorBuilder: (
          BuildContext context, int index) {
            return SizedBox(
              height: 30,
            );
          },
        padding: const EdgeInsets.all(40),
        itemCount: historic.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            child: Container(
              height: 50,
              decoration: BoxDecoration(
                color : Colors.blueAccent,
                borderRadius: BorderRadius.all(
                    Radius.circular(50.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 10.0,
                    spreadRadius: 5.0,
                    offset: Offset(
                      5.0,
                      5.0,
                    ),
                  )
                ],
              ),
              alignment: Alignment(0, 0),
              child: Text(historic[index],
                      style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
              // child: Row(
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   mainAxisAlignment: MainAxisAlignment.center,
                // children: <Widget>[
                //   Text("test",
                //     style: TextStyle(
                //       fontSize: 12,
                //       color: Colors.white,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                //   SizedBox(width: 20),
                //   Icon(
                //     Icons.arrow_forward,
                //     color: Colors.white,
                //   ),
                //   SizedBox(width: 20),
                //   Text("test",
                //     style: TextStyle(
                //       fontSize: 12,
                //       color: Colors.white,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   )
                // ],
              // ),
            ),
          );
        }
      ),
    );
  }
}