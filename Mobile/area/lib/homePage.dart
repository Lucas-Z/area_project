import 'package:area/inAppWebView.dart';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:area/global.dart';
import 'package:http/http.dart';
import 'package:requests/requests.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';


class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<int> services = [];
  int counter = 0;

  void getSessionInfo() async {
    List<int> service = [];
    var url = "http://"+ ipserver +":8080/services/infos/";
    var response = await Requests.get(url);
    response.raiseForStatus();
    for (var i = 0; i < response.json().length; i++) {
      service.add(entries.indexOf(response.json()[i]["name"].toLowerCase()));
    }
    if ((services.length == 0 && service.length != 0) || services.length != service.length) {
      setState(() {
        services = service;
      });
    }
  }

  void removeService(String serviceName) async {
    print(serviceName);
    var url = "http://"+ ipserver +":8080/services/remove?name="+ serviceName;
    await Requests.get(url);
    getSessionInfo();
  }

  void addServices(String services) async {
    String hostname = Requests.getHostname("http://"+ ipserver +":8080/");
    var cookies = await Requests.getStoredCookies(hostname);
    print(cookies);
    Navigator.pushNamed(
      context,
      '/webView',
      arguments: AppWebViewArguments(
        services,
        cookies["token"],
      ),
    );
    getSessionInfo();
  }

  final List<String> entries = <String>['discord', 'trello', 'spotify', 'github', 'mailjet', 'deezer', 'openweather'];
  final List<Color> colors = [Color.fromRGBO(121, 141, 207, 1),
                              Color.fromRGBO(0, 121, 191, 1),
                              Color.fromRGBO(30, 215, 96, 1),
                              Color.fromRGBO(0, 0, 0, 1),
                              Color.fromRGBO(255, 174, 0, 1),
                              Color.fromRGBO(54, 197, 242, 1),
                              Color.fromRGBO(235, 110, 75, 1)];

  final List<Image> icons = [Image.asset("assets/discord.png"),
                            Image.asset("assets/trello.png"),
                            Image.asset("assets/spotify.png"),
                            Image.asset("assets/logoGithub.png"),
                            Image.asset("assets/mailjet.png"),
                            Image.asset("assets/deezer.png"),
                            Image.asset("assets/openweather.png"),
                            ];

  void disconnection() async {
    historic = [];
    var url = "http://"+ ipserver +":8080/auth/logout/";
    var response = await Requests.get(url);
    print(response.statusCode);
    if (response.statusCode == 200)
      Navigator.pushNamed(context, '/');
  }

  @override
  Widget build(BuildContext context) {
    getSessionInfo();
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar (
        backgroundColor: Colors.grey[800],
          centerTitle: true,
          title: Text("Home"),
        ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text
              ('Profile',
              style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              decoration: BoxDecoration(
                color: Colors.blueAccent,
              ),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Trigger'),
              onTap: () {
                Navigator.pushNamed(context, '/trigger');
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Historic'),
              onTap: () {
                Navigator.pushNamed(context, '/historic');
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Add trigger'),
              onTap: () {
                Navigator.pushNamed(context, '/addTrigger');
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Disconnection'),
              onTap: () {
                disconnection();
              },
            ),
          ],
        ),
      ),
      body: ListView.separated (
        separatorBuilder: (
          BuildContext context, int index) {
            return SizedBox(
              height: 30,
            );
          },
        padding: const EdgeInsets.all(40),
        itemCount: services.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onLongPress: () => removeService(entries[services[index]]),
            child: Container(
              height: 100,
              decoration: BoxDecoration(
                color : colors[services[index]],
                borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 10.0,
                    spreadRadius: 5.0,
                    offset: Offset(
                      5.0,
                      5.0,
                    ),
                  )
                ],
                border: Border.all(
                  color: colors[services[index]],
                  width: 6,
                ),
              ),
              child: Row(
                children: <Widget>[
                  SizedBox(width: 70),
                  Container(
                    height: 70,
                    width: 70,
                    child: Column(
                      children: <Widget>[
                        icons[services[index]],
                      ],
                    ),
                  ),
                  SizedBox(width: 30),
                  Text('${entries[services[index]]}',
                    style: TextStyle(
                      fontSize: 17,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            ),
          );
        }
      ),
      floatingActionButton: SpeedDial(
          animatedIcon: AnimatedIcons.menu_close,
          animatedIconTheme: IconThemeData(size: 22),
          backgroundColor: Colors.blueAccent,
          visible: true,
          curve: Curves.bounceIn,
          children: [
                SpeedDialChild(
                child: Icon(Icons.add_circle),
                backgroundColor: Colors.blueAccent,
                onTap: () => addServices('discord'),
                label: 'Discord',
                labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0),
                ),
                SpeedDialChild(
                child: Icon(Icons.add_circle),
                backgroundColor: Colors.blueAccent,
                onTap: () => addServices('trello'),
                label: 'Trello',
                labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0),
                ),
                SpeedDialChild(
                child: Icon(Icons.add_circle),
                backgroundColor: Colors.blueAccent,
                onTap: () => addServices('spotify'),
                label: 'Spotify',
                labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0),
                ),
                SpeedDialChild(
                child: Icon(Icons.add_circle),
                backgroundColor: Colors.blueAccent,
                onTap: () => addServices('github'),
                label: 'GitHub',
                labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0),
                ),
                SpeedDialChild(
                child: Icon(Icons.add_circle),
                backgroundColor: Colors.blueAccent,
                onTap: () => addServices('mailjet'),
                label: 'Mailjet',
                labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0),
                ),
                SpeedDialChild(
                child: Icon(Icons.add_circle),
                backgroundColor: Colors.blueAccent,
                onTap: () => addServices('deezer'),
                label: 'Deezer',
                labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0),
                ),
                SpeedDialChild(
                child: Icon(Icons.add_circle),
                backgroundColor: Colors.blueAccent,
                onTap: () => addServices('openweather'),
                label: 'OpenWeather',
                labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0),
                ),
         ],
      ),
    );
  }
}
