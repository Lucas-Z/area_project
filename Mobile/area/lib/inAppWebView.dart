import 'dart:async';
import 'global.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';


class AppWebViewArguments {
  final String service;
  final String token;

  AppWebViewArguments(this.service, this.token);
}

class AppWebView extends StatefulWidget {
  @override
  _AppWebViewState createState() => _AppWebViewState();
}

class _AppWebViewState extends State<AppWebView> {

    InAppWebViewController webView;
    final CookieManager manager = CookieManager();

    @override
  Widget build(BuildContext context) {
    final AppWebViewArguments services = ModalRoute.of(context).settings.arguments;
    manager.deleteAllCookies();
    manager.setCookie(url: "http://"+ ipserver +":8080/", name: "token", value: services.token);
    return Scaffold(
      body: InAppWebView(
        initialOptions: InAppWebViewWidgetOptions(),
        initialUrl: "http://"+ ipserver +":8080/services/add/?name="+ services.service,
        onWebViewCreated: (InAppWebViewController controller) {
          webView = controller;
        },
        onLoadStop: (InAppWebViewController controller, String url) {
          if (url.indexOf('/services/') != -1 && url.indexOf('/auth/callback') != -1) {
            Navigator.pushReplacementNamed(context, '/home');
          } else if (url.indexOf('/services/add/alreadySub') != -1 || url.indexOf("http://" + ipserver + ":8081/") != -1) {
            Navigator.pushReplacementNamed(context, '/home');
          }
        },
      )
    );
  }
}