import 'package:flutter/material.dart';
import 'package:area/global.dart';
import 'package:requests/requests.dart';

class LoginPage extends StatefulWidget{
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{

  String mail;
  String password;

  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Error"),
          content: Text("Wrong mail or password."),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _onPressedHome() async {
    var url = "http://"+ ipserver +":8080/auth/login/";
    Response response = await Requests.post(url, body: {
    "username": mail,
    "password": password,
    }, headers: {"content-type": "application/x-www-form-urlencoded"});
    print(response.statusCode);
    if (response.statusCode == 200) {
      Navigator.pushNamed(context, '/home');
    } else {
      _showDialog();
    }
  }

  void _onPressedRegister() {
    Navigator.pushNamed(context, '/register');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "AREA",
                style: TextStyle(
                  fontSize: 50,
                  color: Colors.grey[800],
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 50),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Email'
                ),
                onChanged: (text) {
                  mail = text;
                },
              ),
              TextField(
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Password'
                ),
                onChanged: (text) {
                  password = text;
                },
              ),
              SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ButtonTheme(
                  minWidth: 50.0, height : 30.0,
                  child :RaisedButton(
                    child: Text( "Sign In", style: TextStyle(fontSize: 16.0),),
                    textColor: Colors.white, color: Colors.grey[800],
                    onPressed: _onPressedHome,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0))
                    ),
                  ),
                  SizedBox(width: 100),
                  FlatButton(
                    onPressed: _onPressedRegister,
                    child: Text("Sign up ?"),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}