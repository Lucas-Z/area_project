import 'package:flutter/material.dart';
import 'package:area/route.dart';
import 'dart:async';
import 'package:area/global.dart';
import 'package:requests/requests.dart';

void main() {
  Timer.periodic(Duration(seconds: delayCheckTrigger), (timer) async {
    List<String> trigger = [];
    var url = "http://"+ ipserver +":8080/services/checkTriggers";
    var response = await Requests.get(url);
    response.raiseForStatus();
    response.json();
    for (var i = 0; i < response.json().length; i++) {
      trigger.add(response.json()[i]);
    }
    historic = historic + trigger;
  });
  runApp(RouteWidget());
}