library mobile.reactions;

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

List<String> actionRoute = [
  "/services/github/action/commit",
  "/services/github/action/clone",
  "/services/deezer/action/followers",
  "/services/deezer/action/recommendations",
  '/services/discord/action/members',
  '/services/discord/action/channels',
  '/services/discord/action/channels/message',
  "/services/spotify/action/currentTrack",
  "/services/spotify/action/topArtist",
  "/services/openweather/action/temp",
  "/services/openweather/action/weatherforecast",
];

List<String> actionDesc = [
  "new github commit",
  "new github clone",
  "new deezer follower",
  "new deezer recommendations",
  "new discord member",
  "new discord channel",
  "new discord message",
  "new spotify current track",
  "new spotify top artist",
  "new openweather temperature",
  "new openweather weather forecast",
];

List<String> reactionRoute = [
  "/services/discord/reaction/message",
  "/services/discord/reaction/role",
  "/services/github/reaction/merge",
  "/services/mailjet/reaction/sendmail",
  "/services/trello/reaction/notif",
  "/services/spotify/reaction/playback/skip",
  "/services/spotify/reaction/playback/state",
];

List<String> reactionDesc = [
  "post new discord message",
  "add a role to a discord user",
  "merge a github repo",
  "send a mail",
  "clear all trello notifications",
  "skip current track on spotify",
  "change the player state on spotify",
];