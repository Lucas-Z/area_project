import 'package:area/global.dart';
import 'package:flutter/material.dart';
import 'package:requests/requests.dart';

class RegisterPage extends StatefulWidget{
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage>{

  String mail;
  String password;
  String samePassword;

  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Error"),
          content: Text("Passwords are different"),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _onPressed() async {
    print("Register");
    if (password != samePassword) {
      _showDialog();
    } else {
      var url = "http://"+ ipserver +":8080/auth/signup";
      Response response = await Requests.post(url, body: {
        "username": mail,
        "password": password,
        },
        bodyEncoding: RequestBodyEncoding.FormURLEncoded,
        headers: {"content-type": "application/x-www-form-urlencoded"});
      print(response.statusCode);
      Navigator.pushNamed(context, '/home');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "AREA",
                style: TextStyle(
                  fontSize: 50,
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 50),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Email'
                ),
                onChanged: (text) {
                  mail = text;
                },
              ),
              TextField(
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Password'
                ),
                onChanged: (text) {
                  password = text;
                },
              ),
              TextField(
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Confirm password'
                ),
                onChanged: (text) {
                  samePassword = text;
                },
              ),
              SizedBox(height: 50),
              Row(
                children: <Widget>[
                  ButtonTheme(
                    minWidth: 50.0, height : 30.0,
                    child :RaisedButton(
                      child: Text( "Register", style: TextStyle(fontSize: 16.0),),
                      textColor: Colors.white, color: Colors.blueAccent,
                      onPressed: _onPressed,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0))
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}