import 'package:area/googleWebview.dart';
import 'package:flutter/material.dart';
import 'package:area/homePage.dart';
import 'package:area/loginPage.dart';
import 'package:area/registerPage.dart';
import 'package:area/connectionPage.dart';
import 'package:area/githubWebview.dart';
import 'package:area/discordService.dart';
import 'package:area/inAppWebView.dart';
import 'package:area/Trigger.dart';
import 'package:area/historic.dart';
import 'package:area/addtrigger.dart';

class RouteWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Epicture',
      initialRoute: '/',
      routes: {
        '/': (_) => ConnectionPage(),
        '/login': (_) => LoginPage(),
        '/home': (_) => MyHomePage(),
        '/register': (_) => RegisterPage(),
        '/auth/google': (_) => GoogleWebView(),
        '/auth/github': (_) => GithubWebView(),
        '/webView': (context) => AppWebView(),
        '/addTrigger': (_) => AddTriggerPage(),
        '/trigger': (_) => TriggerPage(),
        '/historic': (_) => HistoricPage(),
      },
    );
  }
}