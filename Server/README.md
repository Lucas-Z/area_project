# API Documentation

---

## Auth


### Google :

### /auth/google (get)
OAuth with google

### /auth/google/callback (get)
**Do not use**
Callback function for google OAuth


### Github :

### /auth/github (get)
OAuth with github


### Local :

### /auth/signup (post)
Create an account with email and password
Required parameters :
* username : the email (in the form of : something@something.something)
* password

### /auth/login (post)
Login to and account with email and password
Required parameters :
* username : the email (in the form of : something@something.something)
* password

### /auth/logout (get)
Logout from the actual account

---

## Services

### /services/infos (get)
Return all the services the actual account has activated

### /services/add (get)
Add a service to the actual account
Required parameters :
* name : the name of the service to add

### /services/remove (get)
Remove a service from the actual account
Required parameters :
* name : the name of the service to remove


### Github :

### /services/github/auth (get)
OAuth for service configuration

### /services/github/redirect (get)
**do not use**
Redirect on the good callback : either to login or to configure the service

### /services/github/action/commit (get)
Get the new commit of a repository
Required parameters :
* owner : the owner of the repository
* repo : the name of the repository

### /services/github/action/clone (get)
Get the number of clone of a repository
Required parameters :
* owner : the owner of the repository
* repo : the name of the repository

### /services/github/reaction/clone (post)
Merge two branches of a repository
Required parameters :
* owner : the owner of the repository
* repo : the name of the repository
* base : the base branch
* head : the head branch
* commit_message : the message of the commit


### Trello :

### /services/trello/auth (get)
OAuth for service configuration

### /services/trello/auth/callback (get)
**Do not use**
Callback function for trello OAuth

### /services/trello/reaction/notif (post)
Clear all trello notifications


### Discord :

### /services/discord/auth (get)
OAuth for service configuration

### /services/discord/auth/callback (get)
**Do not use**
Callback function for discord OAuth


### Spotify :

### /services/spotify/auth (get)
OAuth for service configuration

### /services/spotify/auth/callback (get)
**Do not use**
Callback function for spotify OAuth


### Deezer :

### /services/deezer/auth (get)
OAuth for service configuration

### /services/deezer/auth/callback (get)
**Do not use**
Callback function for deezer OAuth

### /services/deezer/action/followers (get)
Get the number of followers

### /services/deezer/action/recommendations (get)
Get the artists recommended by deezer

### /services/deezer/reaction/notification (post)
TO DO


### Mailjet :

### /services/mailjet/auth (get)
OAuth for service configuration

### /services/mailjet/reaction/sendmail (post)
Send mail to someone
Required parameters :
* emailTo : email of the target
* name : name of the target
* subject : subject of the email
* message : message of the email


### Trigger :

### /services/checkTriggers (get)
Check the action / reaction of all user's triggers
Return an array of bool. Each bool tell us if the reaction was triggered or not.

### /services/getTriggers (get)
Get the array of triggers

### /services/addTrigger (post)
Add a new trigger
Required parameters :
* trigger : a trigger object

### /services/removeTrigger (post)
Remove a trigger
Required parameters :
* triggerIdx : the index of the trigger in the trigger array