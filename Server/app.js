var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var bodyParser = require('body-parser');
var glob = require('./globals');

var indexRouter = require('./routes/index');
var userRouter = require('./routes/user');
var authenticateRouter = require('./routes/authenticate');
var servicesRouter = require('./routes/services');

var app = express();


app.use(passport.initialize());
app.use(cookieSession({
  name: 'session',
  keys: ['tfy3h43ft8h43f8t4h3t8h453tdh4d8g7fd8h']
}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "http://" + glob.WEB_IP + ":8081");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header("Access-Control-Allow-Headers", "Origin,Content-Type, Authorization, x-id, Content-Length, X-Requested-With");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// routes
app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/auth', authenticateRouter);
app.use('/services', servicesRouter);
app.get('/about.json', function(req, res) {
  res.send(
      {
          client: {
              host: req.connection.remoteAddress.replace('::ffff:', '')
          },
          server: {
              current_time: Math.floor(Date.now() / 1000),
              services: [{
                name: "deezer",
                actions: [{
                  name: "get_deezer_followers",
                  description: "The user got a new follower"
                }, {
                  name: "get_deezer_recommendations",
                  description: "The user got a new artist in his deezer recommendations"
                }]
              }, {
                name: "github",
                actions: [{
                  name: "get_github_commit",
                  description: "Check if the repo got a new commit"
                }, {
                  name: "get_github_clone",
                  description: "Check if the repo got a new clone"
                }],
                reactions: [{
                  name: "github_merge",
                  description: "Merge a repo"
                }]
              }, {
                name: "discord",
                actions: [{
                  name: "new_discord_member",
                  description: "The server got a new member"
                }, {
                  name: "new_discord_channel",
                  description: "The server got a new channel"
                }, {
                  name: "new_discord_message",
                  description: "The channel got a new message"
                }],
                reactions: [{
                  name: "send_discord_message",
                  description: "Send a message"
                }, {
                  name: "add_discord_role",
                  description: "Add a role to someone"
                }]
              }, {
                name: "spotify",
                actions: [{
                  name: "new_spotify_current_track",
                  description: "The current track changed"
                }, {
                  name: "new_spotify_top_artist",
                  description: "The top artist changed"
                }],
                reactions: [{
                  name: "skip_track_spotify",
                  description: "skip the current track"
                }, {
                  name: "state_spotify",
                  description: "Change the state of the spotify player"
                }]
              }, {
                name: "openweather",
                actions: [{
                  name: "new_openweather_temperature",
                  description: "The temperature changed"
                }, {
                  name: "new_openweather_forecast",
                  description: "There is new previsions"
                }]
              }, {
                name: "trello",
                reactions: [{
                  name: "clear_notifications",
                  description: "Clear all trello's notifications"
                }]
              }, {
                name: "mailjet",
                reactions: [{
                  name: "send_mail",
                  description: "Send a new email"
                }]
              }]
          }
      }
  );
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;