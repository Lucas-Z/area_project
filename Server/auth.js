const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const GitHubStrategy = require('passport-github2').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const TrelloStrategy = require('passport-trello').Strategy;
const DiscordStrategy = require('passport-discord').Strategy;
const SpotifyStrategy = require('passport-spotify').Strategy;
const DeezerStrategy = require('passport-deezer').Strategy;
const TelegramStrategy = require('passport-telegram').Strategy;
const User = require('./models/User');
const passwordHash = require('password-hash');
const glob = require('./globals');
var {bot} = require ('./routes/services/discord');

module.exports = (passport) => {
    passport.serializeUser((user, done) => {
        done(null, user);
    });

    passport.deserializeUser((user, done) => {
        done(null, user);
    });

    passport.use(new GoogleStrategy({
            clientID: "29112632476-pem4chlj3av94h8aiufn5h2h08sm6uvi.apps.googleusercontent.com",
            clientSecret: "wq6Y_8hM8umbxYhZBkleF4Qh",
            callbackURL: "http://" + glob.SERVER_IP + ":8080/auth/google/callback"
        },
        (token, refreshToken, profile, done) => {
            bot.connect();
            return done(null, {
                profile: profile,
                token: token
            });
        }
    ));

    passport.use(new GitHubStrategy({
            clientID: "b88d4a6f6c29c535d703",
            clientSecret: "af20f1288c6a9f6ef14dccac19cb311f01cc8334",
            callbackURL: "http://" + glob.SERVER_IP + ":8080/services/github/redirect",
            passReqToCallback: true
        },
        (req, token, refreshToken, profile, done) => {
            return done(null, {
                origin: req.query.state,
                profile: profile,
                token: token
            });
        }
    ));

    passport.use(new TrelloStrategy({
            consumerKey: "72816598a6efc8eba92e0da7c0df79d9",
            consumerSecret: "a318ad46550007cc5d6eb949ce2dc04833f20fff521b85603660ab6e61d1e34d",
            callbackURL: "http://" + glob.SERVER_IP + ":8080/services/trello/auth/callback",
            trelloParams: {
                scope: "read,write,account",
                name: "Area",
                expiration: "never"
            }
        },
        (token, refreshToken, profile, done) => {
            return done(null, {
                profile: profile,
                token: token
            });
        }
    ));

    passport.use(new LocalStrategy(
        (email, password, done) => {
            User.findOne({ email: email, signWith: 'local' }, function (err, user) {
                if (err) { return done(err); }
                if (!user) { return done(null, false); }
                if (passwordHash.verify(password, user.password) === false) { return done(null, false); }
                return done(null, user);
            });
        }
      ));

    passport.use(new DiscordStrategy({
        clientID: '676812838513147924',
        clientSecret: 'KRgMhcM_dfc8p51c7c96Hx4V4IVqf-UB',
        callbackURL: 'http://' + glob.SERVER_IP + ':8080/services/discord/auth/callback',
    },
    (accessToken, refreshToken, profile, done) => {
        return done(null, {
            profile: profile,
            token: accessToken
        });
    }));

    passport.use(new SpotifyStrategy({
            clientID: '755eddf0799447d0a477bb34ea721857',
            clientSecret: '187dbbc0ac7546a7b84519b4ab045900',
            callbackURL: 'http://' + glob.SERVER_IP + ':8080/services/spotify/auth/callback',
        },
        (accessToken, refreshToken, profile, done) => {
            return done(null, {
                profile: profile,
                token: accessToken
            });
        }));

    passport.use(new DeezerStrategy({
        clientID: '395224',
        clientSecret: 'efda2a22c168b72595d46acf68f1a62e',
        callbackURL: 'http://' + glob.SERVER_IP + ':8080/services/deezer/auth/callback',
      },
      (accessToken, refreshToken, profile, done) => {
        return done(null, {
            profile: profile,
            token: accessToken
        });
      }));
};