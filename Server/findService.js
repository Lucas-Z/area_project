const findService = function(serviceList, serviceName) {
    return new Promise((resolve, rejects) => {
        serviceList.forEach((elem) => {
            if (elem.name.toLowerCase() === serviceName)
                resolve(elem);
        });
        rejects();
    });
};

module.exports = {findService};