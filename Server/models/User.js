let mongoose = require('mongoose');
mongoose.connect("mongodb+srv://admin:admin@area-ztuc6.mongodb.net/AreaDB?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });
let Schema = mongoose.Schema;

let UserSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    sessionToken: {
        type: String,
        required: true,
    },
    service: {
        type: Array,
        required: false
    },
    signWith: {
        type: String,
        required: true
    },
    triggers: {
        type: Array,
        required: false
    },
});

let User = mongoose.model('Users', UserSchema);

module.exports = User;