var express = require('express');
var router = express.Router();
var passport = require('passport');
var auth = require('../auth');
var jwt = require('jsonwebtoken');
var passwordHash = require('password-hash');
var User = require('../models/User');
var {bot} = require('./services/discord');
var glob = require('../globals');

auth(passport);

router.get('/', (req, res) => {
    res.json({
        status: 'session cookie not set'
    });
});

router.get('/google', passport.authenticate('google', {
    scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
    ]
}));

router.get('/google/callback',
    passport.authenticate('google', {
        failureMessage: 'Google Failed !'
    }),
    async (req, res) => {
        req.session.token = req.user.token;
        const user = new User({email: req.user.profile._json.email, password: ' ',
        sessionToken: req.user.token, service: [], signWith: req.user.profile.provider});
        await User.findOne({ email: req.user.profile._json.email, signWith: req.user.profile.provider}, async (err, u) => {
            if (err || !u) {
                user.save();
            } else {
                await u.updateOne({ sessionToken: req.user.token}, e => {
                    if (e) { console.log(e); }
                });
            }
        });
        res.cookie('token', req.user.token);
        res.redirect("http://" + glob.WEB_IP + ":8081/Services");
    }
);

router.get('/github', passport.authenticate('github', {
    scope: ['user:email'],
    state: 'login'
}));

router.post('/signup', async (req, res) => {
    if (req.body.username != "" && req.body.password != "" && req.body.password.length > 4) {
        if (req.body.username.split('@').length > 1 && req.body.username.split('@')[1].split('.').length > 1) {
            req.session.token = jwt.sign({username: req.body.username}, 'se3f54se3f54se348rd7h8t4fz648tgr384d8');
            const user = new User({email: req.body.username, password: passwordHash.generate(req.body.password),
            sessionToken: req.session.token,
            service: [], signWith: 'local'});
            await user.save();
            res.cookie('token', req.session.token);
            res.status(201).send('ok');
        } else
            res.status(400).send('error');
    } else {
        res.status(400).send('error');
    }
});

router.post('/login',
    passport.authenticate('local', {
        failureMessage: 'Login Failed !'
    }),
    (req, res) => {
        req.session.token = req.user.sessionToken;
        res.cookie('token', req.session.token);
        res.status(200).send('ok');
    }
);

router.get('/logout', (req, res) => {
    try {
        req.logout();
    } catch {}
    finally {}
    if (req.session)
        req.session = null;
    res.cookie('token', '');
    bot.disconnect();
    res.status(200).send('ok');
});

module.exports = router;