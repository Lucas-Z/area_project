var express = require('express');
var router = express.Router();
var verifyToken = require('../verify_token');

/* GET home page. */
router.get('/', function(req, res, next) {
  verifyToken(req.cookies.token).then(isTokenValid => {
    if (isTokenValid)
      res.render('index', {title: "Express"});
    else
      res.render('notconnected');
  });
});

module.exports = router;
