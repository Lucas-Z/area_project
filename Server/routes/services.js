const express = require('express');
const router = express.Router();
const User = require('../models/User');
const passport = require('passport');
const auth = require('../auth');
const {Trigger} = require('../trigger');
const {findService} = require('../findService');
const glob = require('../globals');
const {authTrello, callbackTrello, postClearNotifTrello} = require('./services/trello');
const {callbackDiscord, authDiscord, getNewMember, getNewChannel, getNewMessage, postNewMessage, postNewRole} = require('./services/discord');
const {callbackSpotify, authSpotify, getCurrentTrack, getTopArtist, postSkipPlayback, postPlaybackState} = require('./services/spotify');
const {authGithub, servicesCallbackGithub, authCallbackGithub, getCommitGithub, getCloneGithub, postMergeGithub} = require('./services/github');
const {callbackDeezer, authDeezer, getFollowers, getArtistsRecommendations} = require('./services/deezer');
const {authMailjet, servicesMailjet, postSendmailMailjet} = require('./services/mailjet');
const {servicesOpenweather, authOpenweather, getTempOpenWeather, getWeatherForecast} = require('./services/openweather');

auth(passport);

const array_of_functions = [
  authDiscord,
  authTrello,
  authSpotify,
  authGithub,
  authDeezer,
  authMailjet,
  authOpenweather
];

const array_of_names = [
  "discord",
  "trello",
  "spotify",
  "github",
  "deezer",
  "mailjet",
  "openweather"
];

const action_route = [
  "/services/github/action/commit",
  "/services/github/action/clone",
  "/services/deezer/action/followers",
  "/services/deezer/action/recommendations",
  '/services/discord/action/members',
  '/services/discord/action/channels',
  '/services/discord/action/channels/message',
  "/services/spotify/action/currentTrack",
  "/services/spotify/action/topArtist",
  "/services/openweather/action/temp",
  "/services/openweather/action/weatherforecast",
];
const action_desc = [
  "new github commit",
  "new github clone",
  "new deezer follower",
  "new deezer recommendations",
  "new discord member",
  "new discord channel",
  "new discord message",
  "new spotify current track",
  "new spotify top artist",
  "new openweather temperature",
  "new openweather weather forecast",
];
const reaction_route = [
  "/services/discord/reaction/message",
  "/services/discord/reaction/role",
  "/services/github/reaction/merge",
  "/services/mailjet/reaction/sendmail",
  "/services/trello/reaction/notif",
  "/services/spotify/reaction/playback/skip",
  "/services/spotify/reaction/playback/state",
];
const reaction_desc = [
  "post new discord message",
  "add a role to a discord user",
  "merge a github repo",
  "send a mail",
  "clear all trello notifications",
  "skip current track on spotify",
  "change the player state on spotify",
];

router.get('/', (req, res) => {
});

router.get('/infos', (req, res) => {
  User.findOne({sessionToken: req.cookies.token}, (err, user) => {
    if (err || user === undefined || user === null)
      res.status(400).send('user not found');
    else {
      res.status(200).send(user.service);
    }
  })
});


// TRIGGER

const checkAllTriggers = async (triggers, sessionToken, idx) => {
  return new Promise((resolve, reject) => {
    const t = new Trigger(triggers[idx].action, triggers[idx].reaction, idx);
    t.check(sessionToken).then(val => {
      let toAdd = null;
      if (val == true) {
        toAdd = "You got a " + action_desc[action_route.indexOf(triggers[idx].action.route)] + " so it " + reaction_desc[reaction_route.indexOf(triggers[idx].reaction.route)];
      }
      if (idx === triggers.length - 1) {
        if (toAdd !== null)
          resolve([toAdd]);
        else
          resolve([]);
      } else {
        checkAllTriggers(triggers, sessionToken, idx + 1).then(result => {
          if (toAdd !== null)
            resolve([toAdd].concat(result));
          else
            resolve(result);
        });
      }
    });
  });
};

router.get('/checkTriggers', (req, res) => {
  User.findOne({sessionToken: req.cookies.token}, (err, u) => {
    if (err || u === undefined || u === null)
      res.status(400).send('user not found');
    else {
      if (u.triggers.length > 0) {
        checkAllTriggers(u.triggers, req.cookies.token, 0).then(result => {
          res.status(200).send(result);
        }).catch(e => {
          res.status(400).send(e);
        });
      } else
        res.status(200).send([]);
    }
  });
});

router.post('/addTrigger', (req, res) => {
  User.findOne({sessionToken: req.cookies.token}, async (err, u) => {
    if (err || u === undefined || u === null) {
      res.status(400).send('user not found');
    } else {
      if (req.body.trigger) {
        u.triggers.push(req.body.trigger);
        await u.updateOne({triggers: u.triggers}, async (err) => {
          if (err) {
            res.status(400).send(err);
          } else
            res.status(200).send('Trigger added !');
        });
      } else
        res.status(400).send(err);
    }
  });
});

router.get('/getTriggers', (req, res) => {
  User.findOne({sessionToken: req.cookies.token}, async (err, u) => {
    if (err || u === undefined || u === null) {
      res.status(400).send('user not found');
    } else {
      res.status(200).send(u.triggers);
    }
  });
});

const getTriggersDesc = async (triggers) => {
  return new Promise((resolve, reject) => {
    let result = [];
    triggers.forEach(elem => {
      let add = []
      add.push(action_desc[action_route.indexOf(elem.action.route)]);
      add.push(reaction_desc[reaction_route.indexOf(elem.reaction.route)]);
      result.push(add);
    });
    resolve(result);
  });
};

router.get('/getTriggersDesc', (req, res) => {
  User.findOne({sessionToken: req.cookies.token}, async (err, u) => {
    if (err || u === undefined || u === null) {
      console.log('User not found');
      res.status(400).send('user not found');
    } else {
      console.log(u.triggers);
      getTriggersDesc(u.triggers).then(result => {
        res.status(200).send(result);
      }).catch(e => {
        res.status(400).send(e.message);
      })
    }
  });
});

router.post('/removeTrigger', (req, res) => {
  User.findOne({sessionToken: req.cookies.token}, async (err, u) => {
    if (err || u === undefined || u === null) {
      res.status(400).send('user not found');
    } else {
      if (req.body.triggerIdx <= u.triggers.length) {
        u.triggers.splice(req.body.triggerIdx, 1);
        await u.updateOne({triggers: u.triggers}, async (err) => {
          if (err) {
            res.status(400).send(err);
          } else
            res.status(200).send('Trigger removed !');
        });
      } else
        res.status(400).send('wrong trigger index');
    }
  });
});


// ADD A SERVICE


router.get('/add', (req, res) => {
  User.findOne({sessionToken: req.cookies.token}, (err, user) => {
    if (err || user === undefined || user === null)
      res.status(400).send('user not found');
    else {
      findService(user.service, req.query.name).then(elem => {
        res.redirect("http://" + glob.WEB_IP + ":8081/Services");
      }).catch(e => {
        array_of_names.forEach((elem, i) => {
          if (elem === req.query.name) {
            array_of_functions[i](res);
          }
        });
      });
    }
  })
});

// REMOVE A SERVICE


router.get('/remove', (req, res) => {
  User.findOne({sessionToken: req.cookies.token}, (err, user) => {
    if (err || user === undefined || user === null)
      res.status(400).send('user not found');
    else {
      findService(user.service, req.query.name).then(elem => {
        user.service.forEach((elem, i) => {
          if (elem.name.toLowerCase() === req.query.name.toLowerCase()) {
            user.service.splice(i, 1);
            user.updateOne({service: user.service}, (err) => {
              if (err)
                res.status(400).send(err);
              else
                res.status(200).send('Service ' + elem.name + ' deleted !');
            })
          }
        });
      }).catch(e => {
        res.status(400).send('service not found');
      });
    }
  })
});


// GITHUB


router.get('/github/redirect',
  passport.authenticate('github', {
    failureMessage: 'Github Failed'
  }), async (req, res) => {
    if (req.user.origin === 'login') {
      authCallbackGithub(req, res);
    } else if (req.user.origin === 'services') {
      servicesCallbackGithub(req, res);
    } else {
      res.status(400).send('Github Failed');
    }
  }
);

router.get('/github/auth', (req, res) => {
  passport.authenticate('github', {
    scope: ["repo"],
    state: 'services'
  })(req, res)
});

router.get('/github/action/commit', (req, res) => {
  getCommitGithub(req, res);
});

router.get('/github/action/clone', (req, res) => {
  getCloneGithub(req, res);
});

router.post('/github/reaction/merge', (req, res) => {
  postMergeGithub(req, res);
});


// TRELLO


router.get('/trello/auth', passport.authenticate('trello'));

router.get('/trello/auth/callback',
    passport.authenticate('trello', {
        failureMessage: 'Trello Failed'
    }),
    callbackTrello
);

router.post('/trello/reaction/notif', (req, res) => {
  postClearNotifTrello(req, res);
});

// DISCORD


router.get('/discord/action/members', getNewMember);

router.get('/discord/action/channels', getNewChannel);

router.get('/discord/action/channels/message', getNewMessage);

router.post('/discord/reaction/message', postNewMessage);

router.post('/discord/reaction/role', postNewRole);

router.get('/discord/auth', passport.authenticate('discord', {
    scope: ['bot'],
    permission: 2080898303
  })
);

router.get('/discord/auth/callback', passport.authenticate('discord', {
    failureMessage: 'Discord Failed'
  }),
  callbackDiscord
);


// SPOTIFY


router.get('/spotify/auth', passport.authenticate('spotify', {
    scope: ['user-read-email', 'user-read-private', 'user-library-read', 'user-library-modify', 'user-read-currently-playing', 'user-top-read', 'user-read-playback-state', 'user-modify-playback-state'],
    showDialog: true
  })
);

router.get('/spotify/auth/callback', passport.authenticate('spotify', {
      failureMessage: 'Spotify Failed'
    }),
    callbackSpotify
);

router.get('/spotify/action/currentTrack', getCurrentTrack);

router.get('/spotify/action/topArtist', getTopArtist);

router.post('/spotify/reaction/playback/skip', postSkipPlayback);

router.post('/spotify/reaction/playback/state', postPlaybackState);


// DEEZER


router.get('/deezer/auth/', passport.authenticate('deezer'));

router.get('/deezer/auth/callback',
  passport.authenticate('deezer', { failureMessage: 'Deezer Failed' }),
  callbackDeezer
);

router.get('/deezer/action/followers', getFollowers);

router.get('/deezer/action/recommendations', getArtistsRecommendations);


// MAILJET

router.get('/mailjet/auth', (req, res) => {
  res.redirect("/services/mailjet/auth/callback")
});

router.get('/mailjet/auth/callback', servicesMailjet);

router.post('/mailjet/reaction/sendmail', (req, res) => {
  postSendmailMailjet(req, res);
});

// OPEN WEATHER

router.get('/openweather/auth', servicesOpenweather);

router.get('/openweather/action/temp', (req, res) => {
  getTempOpenWeather(req, res);
});

router.get('/openweather/action/weatherforecast', (req, res) => {
  getWeatherForecast(req, res);
});

module.exports = router;
