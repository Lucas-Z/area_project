const User = require('../../models/User');
const axios = require('axios');
const {findService} = require('../../findService');
const glob = require('../../globals');

const callbackDeezer =  async (req, res) => {
  await User.findOne({sessionToken: req.cookies.token}, async (err, user) => {
    if (err || user === undefined || user === null)
      res.status(400).send('user not found');
    else {
      const newService = {
        name: 'Deezer',
        accessToken: req.user.token,
      }
      user.service.push(newService);
      await user.updateOne({service: user.service}, async (err) => {
        if (err)
          res.status(400).send(err);
        else
          res.redirect("http://" + glob.WEB_IP + ":8081/Services");
          // res.status(200).send('Service ' + newService.name + ' added !' );
      })
    }
  })
}

const authDeezer = (res) => {
  res.redirect('/services/deezer/auth');
};

const getFollowers = async (req, res) => {
  await User.findOne({sessionToken: req.query.sessionToken}, (err, user) => {
    if (err || user === undefined || user === null)
      res.status(400).send('user not found');
    else {
      findService(user.service, "deezer").then(elem => {
        axios.get(`https://api.deezer.com/user/me/followers?access_token=${elem.accessToken}`)
        .then((resp) => {
          res.status(resp.status).send(resp.data);
        })
        .catch((error) => {
          console.log(error);
          res.status(error.response.status).send(error.message);
        })
      }).catch(e => {
        res.status(400).send('Service deezer not setup');
      });
    }
  })
};

const handleActionFollowersDeezer = async (data, sessionToken, triggerIdx) => {
  return new Promise(async (resolve, reject) => {
      await User.findOne({sessionToken: sessionToken}, async (err, u) => {
          if (err || u === undefined || u === null)
              resolve(false);
          else {
              if (u.triggers[triggerIdx].data === null) {
                  const newData = {
                      total: data.total
                  };
                  u.triggers[triggerIdx].data = newData;
                  await u.updateOne({triggers: u.triggers}, async (err) => {
                      resolve(false);
                  })
              } else {
                  if (data.total - u.triggers[triggerIdx].data.total >= 1) {
                      u.triggers[triggerIdx].data.total = data.total;
                      await u.updateOne({triggers: u.triggers}, async (err) => {
                          resolve(true);
                      })
                  } else
                      resolve(false);
              }
          }
      });
  });
}

const getArtistsRecommendations = async (req, res) => {
  await User.findOne({sessionToken: req.query.sessionToken}, (err, user) => {
    if (err || user === undefined || user === null)
      res.status(400).send('user not found');
    else {
      findService(user.service, "deezer").then(elem => {
        axios.get(`https://api.deezer.com/user/me/recommendations/artists?access_token=${elem.accessToken}`)
        .then((resp) => {
          res.status(resp.status).send(resp.data);
        })
        .catch(error => {
          console.log(error)
          res.status(error.response.status).send(error.message);
        });
      }).catch(e => {
        res.status(400).send('Service deezer not setup');
      });
    }
  })
};

const keepNamesOnly = async (data) => {
  return new Promise((resolve, reject) => {
    res = [];
    data.forEach((elem) => {
      res.push(elem.name);
    });
    resolve(res);
  });
};

const handleActionRecommendationsDeezer = async (data, sessionToken, triggerIdx) => {
  return new Promise(async (resolve, reject) => {
      await User.findOne({sessionToken: sessionToken}, async (err, u) => {
          if (err || u === undefined || u === null)
              resolve(false);
          else {
              if (u.triggers[triggerIdx].data === null) {
                  let artists = await keepNamesOnly(data.data);
                  const newData = {
                      artists: artists
                  };
                  u.triggers[triggerIdx].data = newData;
                  await u.updateOne({triggers: u.triggers}, async (err) => {
                      resolve(false);
                  })
              } else {
                  let artists = await keepNamesOnly(data.data);
                  if (artists.length === u.triggers[triggerIdx].data.artists.length && artists.every(function(elem, i) {
                      return elem === u.triggers[triggerIdx].data.artists[i];
                  })) {
                      resolve(false);
                  } else {
                      u.triggers[triggerIdx].data.artists = artists;
                      await u.updateOne({triggers: u.triggers}, async (err) => {
                          resolve(true);
                      })
                  }
              }
          }
      });
  });
}

module.exports = {
  callbackDeezer,
  authDeezer,
  getFollowers,
  getArtistsRecommendations,
  handleActionFollowersDeezer,
  handleActionRecommendationsDeezer,
};