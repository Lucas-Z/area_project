const User = require('../../models/User');
const {findService} = require('../../findService');
var Discord = require('discord.io');
const glob = require('../../globals');

var bot = new Discord.Client({
  token: 'Njc2ODEyODM4NTEzMTQ3OTI0.XkxOyg.DFgb8okRMVUjP5RtPKG_BpEdBB8'
})

bot.on('ready', (event) => {
  console.log(`Logged in as ${bot.username} - ${bot.id}`);
});

bot.on('disconnect', (errMsg, code) => {
  console.log(code + ' ' + errMsg);
})

const callbackDiscord =  async (req, res) => {
  await User.findOne({sessionToken: req.query.sessionToken}, async (err, user) => {
    if (err || user === undefined || user === null)
      res.status(400).send('user not found');
    else {
      const newService = {
        name: 'Discord',
        accessToken: req.user.token,
      }
      user.service.push(newService);
      await user.updateOne({service: user.service}, async (err) => {
        if (err)
          res.status(400).send(err);
        else
          res.redirect("http://" + glob.WEB_IP + ":8081/Services");
        // res.status(200).send('Service ' + newService.name + ' added !' );
      })
    }
  })
}

const authDiscord = (res) => {
  res.redirect('/services/discord/auth');
}

const getNewMember = async (req, res) => {
  if (req.query.guild === undefined)
    res.status(400).send('Missing parameters: guild');
  else {
    await User.findOne({sessionToken: req.query.sessionToken}, (err, u) => {
      if (err || u === undefined || u === null)
        res.status(400).send('user not found');
      else {
        findService(u.service, 'discord').then(elem => {
          Object.entries(bot.servers).forEach((server) => {
            if (server[1].name === req.query.guild)
              res.status(200).send(server[1]);
          })
        }).catch(e => res.status(400).send('Service discord not setup'))
      }
    })
  }
}

const handleActionNewMember = async (data, sessionToken, triggerIdx) => {
  return new Promise(async (resolve, reject) => {
    await User.findOne({sessionToken: sessionToken}, async (err, u) => {
      if (err || u === undefined || u === null)
        resolve(false);
      else {
        if (u.triggers[triggerIdx].data === null) {
          u.triggers[triggerIdx].data = data.member_count;
          await u.updateOne({triggers: u.triggers}, async (err) => {
            resolve(false);
          })
        } else {
          if (data.member_count - u.triggers[triggerIdx].data >= 1) {
            u.triggers[triggerIdx].data = data.member_count;
            await u.updateOne({triggers: u.triggers}, async (err) => {
              resolve(true);
            })
          } else
            resolve(false);
        }
      }
    });
  });
}

const getNewChannel = async (req, res) => {
  if (req.query.guild === undefined)
  res.status(400).send('Missing parameters: guild');
  else {
    await User.findOne({sessionToken: req.query.sessionToken}, (err, u) => {
      if (err || u === undefined || u === null)
        res.status(400).send('user not found');
      else {
        findService(u.service, 'discord').then(elem => {
          Object.entries(bot.servers).forEach((server) => {
            if (server[1].name === req.query.guild) {
              res.status(200).send(server[1].channels);
            }
          })
        }).catch(e => res.status(400).send('Service discord not setup'))
      }
    })
  }
}

const handleActionNewChannel = async (data, sessionToken, triggerIdx) => {
  return new Promise(async (resolve, reject) => {
    await User.findOne({sessionToken: sessionToken}, async (err, u) => {
      let len = Object.keys(data).length;
      if (err || u === undefined || u === null)
        resolve(false);
      else {
        if (u.triggers[triggerIdx].data === null) {
          u.triggers[triggerIdx].data = len;
          await u.updateOne({triggers: u.triggers}, async (err) => {
            resolve(false);
          })
        } else {
          if (len - u.triggers[triggerIdx].data >= 1) {
            u.triggers[triggerIdx].data = len;
            await u.updateOne({triggers: u.triggers}, async (err) => {
              resolve(true);
            })
          } else
            resolve(false);
        }
      }
    });
  });
}

const findChannel = (server, channelName) => {
  return new Promise((resolve, rejects) => {
    Object.entries(server.channels).forEach(channel => {
      if (channel[1].name === channelName)
        resolve(channel[1].id);
    })
    rejects();
  })
}

const getNewMessage = async (req, res) => {
  if (req.query.guild === undefined || req.query.channel === undefined)
    res.status(400).send('Missing parameters: guild or channel');
  else {
    await User.findOne({sessionToken: req.query.sessionToken}, (err, u) => {
      if (err || u === undefined || u === null)
        res.status(400).send('user not found');
      else {
        findService(u.service, 'discord').then(elem => {
          Object.entries(bot.servers).forEach((server) => {
            if (server[1].name === req.query.guild) {
              findChannel(server[1], req.query.channel).then(id => {
                bot.getMessages({channelID: id}, (error, array) => {
                  res.status(200).send(array.shift());
                })
              }).catch(e => res.status(400).send('Channel not found'))
            }
          })
        }).catch(e => res.status(400).send('Service discord not setup'))
      }
    })
  }
}

const handleActionNewMessage = async (data, sessionToken, triggerIdx) => {
  return new Promise(async (resolve, reject) => {
    await User.findOne({sessionToken: sessionToken}, async (err, u) => {
      if (err || u === undefined || u === null)
        resolve(false);
      else {
        if (u.triggers[triggerIdx].data === null) {
          u.triggers[triggerIdx].data = data.id;
          await u.updateOne({triggers: u.triggers}, async (err) => {
            resolve(false);
          })
        } else {
          if (data.id !== u.triggers[triggerIdx].data) {
            u.triggers[triggerIdx].data = data.id;
            await u.updateOne({triggers: u.triggers}, async (err) => {
              resolve(true);
            })
          } else
            resolve(false);
        }
      }
    });
  });
}

const postNewMessage = async(req, res) => {
  if (req.body === undefined)
    res.status(400).send('Invalid request');
  else {
    await User.findOne({sessionToken: req.cookies.token}, (err, u) => {
      if (err || u === undefined || u === null)
        res.status(400).send('user not found');
      else {
        findService(u.service, 'discord').then(elem => {
          Object.entries(bot.servers).forEach((server) => {
            if (server[1].name === req.body.guild) {
              findChannel(server[1], req.body.channel).then(id => {
                bot.sendMessage({
                  to: id,
                  message: req.body.message
                }, (error, response) => {
                  res.status(200).send('message sent');
                });
              }).catch(e => res.status(400).send('Channel not found'))
            }
          })
        }).catch(e => res.status(400).send('Service discord not setup'))
      }
    })
  }
}

const findUserInGuild = (server, userID) => {
  return new Promise((resolve, rejects) => {
    Object.entries(server.members).forEach(member => {
      if (member[1].id === userID)
        resolve(member[1].id);
    })
    rejects();
  })
}

const postNewRole = async (req, res) => {
  if (req.body === undefined)
  res.status(400).send('Invalid request');
  else {
    var userID = -1;
    Object.entries(bot.users).forEach(user => {
      if (user[1].username === req.body.user)
        userID = user[1].id;
    })
    if (userID === -1)
      res.status(400).send('Discord user not found');
    await User.findOne({sessionToken: req.cookies.token}, (err, u) => {
      if (err || u === undefined || u === null)
        res.status(400).send('user not found');
      else {
        findService(u.service, 'discord').then(elem => {
          Object.entries(bot.servers).forEach(server => {
            if (server[1].name === req.body.guild) {
              findUserInGuild(server[1], userID).then(ID => {
                Object.entries(server[1].roles).forEach(role => {
                  if (role[1].name.toLowerCase() === req.body.role.toLowerCase()) {
                    bot.addToRole({
                      serverID: server[1].id,
                      userID: ID,
                      roleID: role[1].id
                    }, (error, response) => res.status(200).send('Role added to user'))
                  }
                })
              }).catch(e => res.status(400).send('Discord user not in guild'))
            }
          })
        }).catch(e => res.status(400).send('Service discord not setup'))
      }
    })
  }
}

module.exports = {
  callbackDiscord,
  authDiscord,
  getNewMember,
  handleActionNewMember,
  getNewChannel,
  handleActionNewChannel,
  getNewMessage,
  handleActionNewMessage,
  postNewMessage,
  postNewRole,
  bot
};