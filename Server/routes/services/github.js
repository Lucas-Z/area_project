const Axios = require('axios');
const User = require('../../models/User');
const {findService} = require('../../findService');
const glob = require("../../globals");

const authGithub = (res) => {
    res.redirect('/services/github/auth');
};

const servicesCallbackGithub = async (req, res) => {
    await User.findOne({sessionToken: req.cookies.token}, async (err, u) => {
        if (err || u === undefined || u === null) {
            res.status(400).send('user not found');
        } else {
            const newService = {
                name: 'Github',
                accessToken: req.user.token
            };
            u.service.push(newService);
            await u.updateOne({service: u.service}, async (err) => {
                if (err)
                    res.status(400).send(err);
                else
                    res.redirect("http://" + glob.WEB_IP + ":8081/Services");
                    // res.status(200).send('Service ' + newService.name + ' added !');
            })
        }
    });
}

const authCallbackGithub = async (req, res) => {
    req.session.token = req.user.token;
    const user = new User({email: req.user.profile._json.id, password: ' ',
    sessionToken: req.user.token, service: [], signWith: req.user.profile.provider});
    await User.findOne({ email: req.user.profile._json.id, signWith: req.user.profile.provider}, async (err, u) => {
        if (err || !u) {
            user.save();
        } else {
            await u.updateOne({ sessionToken: req.user.token}, e => {
                if (e) { console.log(e); }
            });
        }
    });
    res.cookie('token', req.user.token);
    res.redirect("http://" + glob.WEB_IP + ":8081/Services");
}

const getCommitGithub = async (req, res) => {
    await User.findOne({sessionToken: req.query.sessionToken}, async (err, u) => {
        if (err || u === undefined || u === null)
            res.status(400).send('user not found');
        else {
            findService(u.service, "github").then(elem => {
                Axios.get(
                    "https://api.github.com/repos/" + req.query.owner + "/" + req.query.repo + "/commits",
                    {headers: {Authorization: "Bearer ".concat(elem.accessToken)}}
                ).then(ret => {
                    res.status(ret.status).send(ret.data);
                }).catch(e => {
                    console.log(e);
                    res.status(e.response.status).send(e.message);
                });
            }).catch(e => {
                res.status(400).send('Service github not setup');
            });
        }
    });
}

const handleActionCommitGithub = async (data, sessionToken, triggerIdx) => {
    return new Promise(async (resolve, reject) => {
        await User.findOne({sessionToken: sessionToken}, async (err, u) => {
            if (err || u === undefined || u === null)
                resolve(false);
            else {
                if (u.triggers[triggerIdx].data === null && data.length > 0) {
                    u.triggers[triggerIdx].data = data[0];
                    await u.updateOne({triggers: u.triggers}, async (err) => {
                        resolve(false);
                    })
                } else if (data.length > 0) {
                    if (u.triggers[triggerIdx].data.sha !== data[0].sha) {
                        u.triggers[triggerIdx].data = data[0];
                        await u.updateOne({triggers: u.triggers}, async (err) => {
                            resolve(true);
                        })
                    } else
                        resolve(false);
                } else
                    resolve(false);
            }
        });
    });
}

const getCloneGithub = async (req, res) => {
    await User.findOne({sessionToken: req.query.sessionToken}, async (err, u) => {
        if (err || u === undefined || u === null)
            res.status(400).send('user not found');
        else {
            findService(u.service, "github").then(elem => {
                Axios.get(
                    "https://api.github.com/repos/" + req.query.owner + "/" + req.query.repo + "/traffic/clones",
                    {headers: {Authorization: "Bearer ".concat(elem.accessToken)}}
                ).then(ret => {
                    res.status(ret.status).send(ret.data);
                }).catch(e => {
                    console.log(e);
                    res.status(e.response.status).send(e.message);
                });
            }).catch(e => {
                res.status(400).send('Service github not setup');
            });
        }
    });
}

const handleActionCloneGithub = async (data, sessionToken, triggerIdx) => {
    return new Promise(async (resolve, reject) => {
        await User.findOne({sessionToken: sessionToken}, async (err, u) => {
            if (err || u === undefined || u === null)
                resolve(false);
            else {
                if (u.triggers[triggerIdx].data === null) {
                    const newData = {
                        count: data.count
                    };
                    u.triggers[triggerIdx].data = newData;
                    await u.updateOne({triggers: u.triggers}, async (err) => {
                        resolve(false);
                    })
                } else {
                    if (data.count - u.triggers[triggerIdx].data.count >= 1) {
                        u.triggers[triggerIdx].data.count = data.count;
                        await u.updateOne({triggers: u.triggers}, async (err) => {
                            resolve(true);
                        })
                    } else
                        resolve(false);
                }
            }
        });
    });
}

const postMergeGithub = async (req, res) => {
    await User.findOne({sessionToken: req.body.sessionToken}, async (err, u) => {
        if (err || u === undefined || u === null)
            res.status(400).send('user not found');
        else {
            findService(u.service, "github").then(elem => {
                Axios.post(
                    "https://api.github.com/repos/" + req.body.owner + "/" + req.body.repo + "/merges",
                    {
                        base: req.body.base,
                        head: req.body.head,
                        commit_message: req.body.commit_message,
                        headers: {Authorization: "Bearer ".concat(elem.accessToken)}
                    }
                ).then(ret => {
                    res.status(ret.status).send(ret.statusText);
                }).catch(e => {
                    console.log(e);
                    res.status(e.response.status).send(e.message);
                });
            }).catch(e => {
                res.status(400).send('Service github not setup');
            });
        }
    });
}

module.exports = {
    authGithub,
    servicesCallbackGithub,
    authCallbackGithub,
    getCommitGithub,
    getCloneGithub,
    postMergeGithub,
    handleActionCommitGithub,
    handleActionCloneGithub,
};