const User = require('../../models/User');
const mailjet = require ('node-mailjet').connect('6c7f7d3b7f55b62c8d842bc013f72093', '3f44a862288591566f3bbdab5badb507');
const {findService} = require("../../findService");
const glob = require('../../globals');

const authMailjet = (res) => {
    res.redirect('/services/mailjet/auth');
}

const servicesMailjet = async (req, res) => {
    await User.findOne({sessionToken: req.cookies.token}, async (err, u) => {
        if (err || u === undefined || u === null) {
            res.status(400).send('user not found');
        } else {
            const newService = {
                name: 'Mailjet'
            }
            u.service.push(newService);
            await u.updateOne({service: u.service}, async (err) => {
                if (err)
                    res.status(400).send(err);
                else
                    res.redirect("http://" + glob.WEB_IP + ":8081/Services");
                    // res.status(200).send('Service ' + newService.name + ' added !');
            })
        }
    });
}

const postSendmailMailjet = async (req, res) => {
    await User.findOne({sessionToken: req.body.sessionToken}, async (err, u) => {
        if (err || u === undefined || u === null)
            res.status(400).send('user not found');
        else {
            findService(u.service, "mailjet").then(elem => {
                const request = mailjet.post("send", {'version': 'v3.1'}).request({
                    "Messages":[
                        {
                            "From": {
                                "Email": "leo.huin@epitech.eu",
                                "Name": "Area"
                            },
                            "To": [
                                {
                                    "Email": req.body.emailTo,
                                    "Name": req.body.name
                                }
                            ],
                            "Subject": req.body.subject,
                            "TextPart": "",
                            "HTMLPart": req.body.message + "</br><a href=\"http://" + glob.SERVER_IP + ":8080\"><h3>Sent from Area</h3></a>",
                            "CustomID": "Area"
                        }
                    ]
                });
                request.then(ret => {
                    if (ret.status)
                        res.status(ret.status).send(ret.statusText);
                    else
                        res.status(200).send("ok");
                }).catch(e => {
                    console.log(e);
                    res.status(400).send(e.message);
                })
            }).catch(e => {
                res.status(400).send('Service mailjet not setup');
            });
        }
    });
}

module.exports = {authMailjet, servicesMailjet, postSendmailMailjet};