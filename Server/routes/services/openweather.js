const User = require('../../models/User');
const openWeather = require('openweather-apis');
const {findService} = require("../../findService");
const glob = require('../../globals');
const axios = require('axios');

const authOpenweather = (res) => {
    res.redirect('/services/openweather/auth');
};

const setupOpenWeather = (query) => {
    openWeather.setAPPID('25b2737398e717e8949ee2d72ab6b14e');
    openWeather.setLang('fr');
    openWeather.setCity(query.city);
    openWeather.setUnits('metric');
};

const servicesOpenweather = async (req, res) => {
    await User.findOne({sessionToken: req.cookies.token}, async (err, user) => {
        if (err || user === undefined || user === null)
            res.status(400).res('user not found');
        else {
            const newService = {
                name: 'OpenWeather'
            };
            user.service.push(newService);
            await user.updateOne({service: user.service}, async (err) => {
                if (err)
                    res.status(400).send(err);
                else
                    res.redirect("http://" + glob.WEB_IP + ":8081/Services");
                    // res.status(200).send('Service ' + newService.name + ' added !' );
            })
        }
    });
};

const getTempOpenWeather = async (req, res) => {
    await User.findOne({sessionToken: req.query.sessionToken}, async (err, u) => {
        if (err || u === undefined || u === null)
            res.status(400).send('user not found');
        else {
            findService(u.service, 'openweather').then(elem => {
                setupOpenWeather(req.query);
                openWeather.getTemperature((err, temp) => {
                    res.status(200).send(`${temp}`);
                })
            }).catch(e => {
                res.status(400).send(e.message);
            })

        }
    })
};

const handleActiongetTempOpenWeather = async (data, sessionToken, triggerIdx) => {
    return new Promise(async (resolve, reject) => {
        await User.findOne({sessionToken: sessionToken}, async (err, u) => {
            if (err || u === undefined || u === null)
                resolve(false);
            else {
                if (u.triggers[triggerIdx].data === null) {
                    u.triggers[triggerIdx].data = data;
                    await u.updateOne({triggers: u.triggers}, async (err) => {
                        resolve(true);
                    })
                } else {
                    if (u.triggers[triggerIdx].data === null) {
                        u.triggers[triggerIdx].data = data;
                        await u.updateOne({triggers: u.triggers}, async (err) => {
                            resolve(false);
                        })
                    } else if (u.triggers[triggerIdx].data > 5 && data <= 5) {
                        u.triggers[triggerIdx].data = data;
                        await u.updateOne({triggers: u.triggers}, async (err) => {
                            resolve(true);
                        })
                    } else if (u.triggers[triggerIdx].data <= 5 && data > 5) {
                        u.triggers[triggerIdx].data = data;
                        await u.updateOne({triggers: u.triggers}, async (err) => {
                            resolve(false);
                        })
                    } else
                        resolve(false);
                }
            }
        });
    })
};

const getWeatherForecast = async (req, res) => {
    await User.findOne({sessionToken: req.query.sessionToken}, async (err, u) => {
        if (err || u === undefined || u === null)
            res.status(400).send('user not found');
        else {
            findService(u.service, 'openweather').then(elem => {
                axios.get(`http://api.openweathermap.org/data/2.5/forecast?q=${req.query.city}&appid=25b2737398e717e8949ee2d72ab6b14e`).then(resp => {
                    res.status(200).send(resp.data.list.slice(0, 2));
                }).catch(e => {
                    res.status(400).send(e.message);
                })
            }).catch(e => {
                res.status(400).send(e.message);
            })
        }
    })
};

const handleActiongetWeatherForecast = async (data, sessionToken, triggerIdx) => {
    return new Promise(async (resolve, reject) => {
        await User.findOne({sessionToken: sessionToken}, async (err, u) => {
            if (err || u === undefined || u === null)
                resolve(false);
            else {
                if (u.triggers[triggerIdx].data === null && data !== undefined && data !== null) {
                    u.triggers[triggerIdx].data = data;
                    await u.updateOne({triggers: u.triggers}, async (err) => {
                        resolve(false);
                    })
                } else if (u.triggers[triggerIdx].data[0].main !== data[0].main) {
                    u.triggers[triggerIdx].data = data;
                    await u.updateOne({triggers: u.triggers}, async (err) => {
                        resolve(true);
                    })
                } else
                    resolve(false);
            }
        })
    })
};

module.exports = {
    servicesOpenweather,
    authOpenweather,
    getTempOpenWeather,
    getWeatherForecast,
    handleActiongetTempOpenWeather,
    handleActiongetWeatherForecast
};