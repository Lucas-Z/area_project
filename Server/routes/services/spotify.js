const User = require ('../../models/User');
const axios = require('axios');
const {findService} = require("../../findService");
const glob = require('../../globals');

const callbackSpotify = async (req, res) => {
  await User.findOne({sessionToken: req.cookies.token}, async (err, user) => {
    if (err || user === undefined || user === null)
      res.status(400).send('user not found');
    else {
      const newService = {
        name: 'Spotify',
        accessToken: req.user.token
      };
      user.service.push(newService);
      await user.updateOne({service: user.service}, async (err) => {
        if (err)
          res.status(400).send(err);
        else
          res.redirect("http://" + glob.WEB_IP + ":8081/Services");
          // res.status(200).send('Service ' + newService.name + ' added !' );
      })
    }
  });
};

const authSpotify = (res) => {
  res.redirect('/services/spotify/auth');
};

const getCurrentTrack = async (req, res) => {
  await User.findOne({sessionToken: req.query.sessionToken}, async (err, u) => {
    if (err || u === undefined || u === null)
      res.status(400).send('user not found');
    else {
      findService(u.service, "spotify").then(elem => {
        axios.get('https://api.spotify.com/v1/me/player/currently-playing',
        {headers: {Authorization: 'Bearer ' + elem.accessToken}}
        ).then(resp => {
          res.status(resp.status).send(resp.data);
        })
        .catch(error => {
          res.status(error.response.status).send(error.message);
        });
      }).catch(e => {
        res.status(400).send('Service spotify not setup');
      });
    }
  })
};

const handleActionCurrentTrackSpotify = async (data, sessionToken, triggerIdx) => {
  return new Promise(async (resolve, reject) => {
      await User.findOne({sessionToken: sessionToken}, async (err, u) => {
          if (err || u === undefined || u === null)
              resolve(false);
          else {
              if (u.triggers[triggerIdx].data === null && data.item !== undefined && data.item !== null) {
                  const newData = {
                      name: data.item.name
                  };
                  u.triggers[triggerIdx].data = newData;
                  await u.updateOne({triggers: u.triggers}, async (err) => {
                      resolve(false);
                  })
              } else if (data.item !== undefined && data.item !== null) {
                  if (u.triggers[triggerIdx].data.name !== data.item.name) {
                      u.triggers[triggerIdx].data.name = data.item.name;
                      await u.updateOne({triggers: u.triggers}, async (err) => {
                          resolve(true);
                      })
                  } else
                      resolve(false);
              } else
                resolve(false);
          }
      });
  });
}

const getTopArtist = async (req, res) => {
  await User.findOne({sessionToken: req.query.sessionToken}, async (err, u) => {
    if (err || u === undefined || u === null)
      res.status(400).send('user not found');
    else {
      findService(u.service, "spotify").then(elem => {
        axios.get('https://api.spotify.com/v1/me/top/artists?limit=1',
        {headers: {Authorization: 'Bearer ' + elem.accessToken}}
        ).then(resp => {
          res.status(resp.status).send(resp.data);
        })
        .catch(error => {
          res.status(error.response.status).send(error.message);
        });
      }).catch(e => {
        res.status(400).send('Service spotify not setup');
      });
    }
  })
}

const handleActionTopArtistSpotify = async (data, sessionToken, triggerIdx) => {
  return new Promise(async (resolve, reject) => {
      await User.findOne({sessionToken: sessionToken}, async (err, u) => {
          if (err || u === undefined || u === null)
              resolve(false);
          else {
              if (u.triggers[triggerIdx].data === null && data.items !== undefined && data.items[0] !== undefined) {
                  const newData = {
                      name: data.items[0].name
                  };
                  u.triggers[triggerIdx].data = newData;
                  await u.updateOne({triggers: u.triggers}, async (err) => {
                      resolve(false);
                  })
              } else if (data.items !== undefined && data.items[0] !== undefined) {
                  if (u.triggers[triggerIdx].data.name !== data.items[0].name) {
                      u.triggers[triggerIdx].data.name = data.items[0].name;
                      await u.updateOne({triggers: u.triggers}, async (err) => {
                          resolve(true);
                      })
                  } else
                      resolve(false);
              }
          }
      });
  });
}

const postSkipPlayback = async (req, res) => {
  if (req.body.skip === undefined)
    res.status(400).send('Missing parameters : skip')
  await User.findOne({sessionToken: req.body.sessionToken}, async (err, u) => {
    if (err || u === undefined || u === null)
      res.status(400).send('user not found');
    else {
      findService(u.service, "spotify").then(elem => {
        axios.post(`https://api.spotify.com/v1/me/player/${req.body.skip}`, {},
        {headers: {Authorization: 'Bearer ' + elem.accessToken}}
        ).then(resp => {
          res.status(resp.status).send(resp.statusText);
        })
        .catch(error => console.log(error))
      }).catch(e => {
        res.status(400).send('Service spotify not setup');
      });
    }
  })
}

const postPlaybackState = async (req, res) => {
  if (req.body.state === undefined)
    res.status(400).send('Missing parameters : state')
  await User.findOne({sessionToken: req.body.sessionToken}, async (err, u) => {
    if (err || u === undefined || u === null)
      res.status(400).send('user not found');
    else {
      findService(u.service, "spotify").then(elem => {
        axios.put(`https://api.spotify.com/v1/me/player/${req.body.state}`, {},
        {headers: {Authorization: 'Bearer ' + elem.accessToken}}
        ).then(resp => {
          res.status(resp.status).send(resp.statusText);
        })
        .catch(error => console.log(error))
      }).catch(e => {
        res.status(400).send('Service spotify not setup');
      });
    }
  })
}

module.exports = {
  callbackSpotify,
  authSpotify,
  getCurrentTrack,
  getTopArtist,
  postSkipPlayback,
  postPlaybackState,
  handleActionCurrentTrackSpotify,
  handleActionTopArtistSpotify
};