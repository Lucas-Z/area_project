const Axios = require('axios');
const User = require('../../models/User');
const {findService} = require("../../findService");
const glob = require('../../globals');

const authTrello = (res) => {
    res.redirect('/services/trello/auth');
}

const callbackTrello = async (req, res) => {
    await User.findOne({sessionToken: req.cookies.token}, async (err, u) => {
        if (err || u === undefined || u === null) {
            res.status(400).send('user not found');
        } else {
            const newService = {
                name: 'Trello',
                accessToken: req.user.token
            }
            u.service.push(newService);
            await u.updateOne({service: u.service}, async (err) => {
                if (err)
                    res.status(400).send(err);
                else
                    res.redirect("http://" + glob.WEB_IP + ":8081/Services");
                    // res.status(200).send('Service ' + newService.name + ' added !');
            })
        }
    });
}

const postClearNotifTrello = async (req, res) => {
    await User.findOne({sessionToken: req.body.sessionToken}, async (err, u) => {
        if (err || u === undefined || u === null)
            res.status(400).send('user not found');
        else {
            findService(u.service, "trello").then(elem => {
                Axios.post(
                    "https://api.trello.com/1/notifications/all/read?key=72816598a6efc8eba92e0da7c0df79d9&token=" + elem.accessToken,
                    {
                        headers: {Authorization: "Bearer ".concat(elem.accessToken)}
                    }
                ).then(ret => {
                    res.status(ret.status).send(ret.statusText);
                }).catch(e => {
                    console.log(e);
                    res.status(e.response.status).send(e.message);
                });
            }).catch(e => {
                res.status(400).send('Service trello not setup');
            });
        }
    });
}

module.exports = {authTrello, callbackTrello, postClearNotifTrello};