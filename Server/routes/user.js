const express = require('express');
const router = express.Router();
var User = require('../models/User');

router.get('/', (req, res) => {
  res.send('respond with a resource');
});

router.get('/infos', (req, res) => {
  User.findOne({sessionToken: req.session.token}, (err, user) => {
    if (err || user === undefined || user === null)
    res.status(400).send('User not found');
    else {
      res.status(200).send(user);
    }
  })
});

module.exports = router;
