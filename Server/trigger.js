const Axios = require('axios');
const glob = require('./globals');
const {handleActionCommitGithub, handleActionCloneGithub} = require('./routes/services/github');
const {handleActionFollowersDeezer, handleActionRecommendationsDeezer} = require('./routes/services/deezer');
const {handleActionNewMember, handleActionNewChannel, handleActionNewMessage} = require('./routes/services/discord');
const {handleActiongetTempOpenWeather, handleActiongetWeatherForecast} = require('./routes/services/openweather');
const {handleActionCurrentTrackSpotify, handleActionTopArtistSpotify} = require('./routes/services/spotify');

const action_route = [
    "/services/github/action/commit",
    "/services/github/action/clone",
    "/services/deezer/action/followers",
    "/services/deezer/action/recommendations",
    '/services/discord/action/members',
    '/services/discord/action/channels',
    '/services/discord/action/channels/message',
    "/services/spotify/action/currentTrack",
    "/services/spotify/action/topArtist",
    "/services/openweather/action/temp",
    "/services/openweather/action/weatherforecast",
];

const handle_action = [
    handleActionCommitGithub,
    handleActionCloneGithub,
    handleActionFollowersDeezer,
    handleActionRecommendationsDeezer,
    handleActionNewMember,
    handleActionNewChannel,
    handleActionNewMessage,
    handleActionCurrentTrackSpotify,
    handleActionTopArtistSpotify,
    handleActiongetTempOpenWeather,
    handleActiongetWeatherForecast,
];

const add_params_to_route = function(route, params, sessionToken) {
    let url = route;
    url += "?" + "sessionToken=" + sessionToken;
    Object.entries(params).forEach((elem) => {
        url += "&" + elem[0] + "=" + elem[1];
    });
    url = "http://" + glob.SERVER_IP + ":" + glob.SERVER_PORT + url;
    return url;
}

const find_handle_action = function(route) {
    return new Promise((resolve, reject) => {
        action_route.forEach((elem, idx) => {
            if (elem === route) {
                resolve(idx);
            }
        });
    });
}

const Trigger = function(action, reaction, triggerIdx) {
    this.action = action;
    this.reaction = reaction;
    this.triggerIdx = triggerIdx;
}

Trigger.prototype.check = async function(sessionToken) {
    return new Promise((resolve, reject) => {
        const action_url = add_params_to_route(this.action.route, this.action.params, sessionToken);
        const reaction_url = "http://" + glob.SERVER_IP + ":" + glob.SERVER_PORT + this.reaction.route;
        this.reaction.params.sessionToken = sessionToken;
        Axios.get(action_url).then(async (res) => {
            // console.log(res.data);
            const idx = await find_handle_action(this.action.route);
            if (idx !== -1 && res.data !== null && res.data !== undefined) {
                handle_action[idx](res.data, sessionToken, this.triggerIdx).then(bool => {
                    if (bool === true) {
                        Axios.post(reaction_url, this.reaction.params).then(res => {
                            resolve(true);
                        }).catch(e => {
                            resolve(false);
                        });
                    } else
                        resolve(false);
                });
            } else
                resolve(false);
        }).catch(e => {
            console.log(e);
            resolve(false);
        });
    });
}

module.exports = {Trigger};