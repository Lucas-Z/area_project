const User = require('./models/User');

module.exports = (token) => {
    return new Promise((resolve, reject) => {
        User.findOne({ sessionToken: token }, function (err, user) {
            if (err) { resolve(false); }
            if (!user) { resolve(false); }
            resolve(true);
        });
    });
};