import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import FormTabs from './Components/FormTabs';
import Home from './Routes/Home';
import './App.css';
import glob from './globals';
import Services from './Components/Services.js';
import Trigger from './Routes/Trigger.js';
import AcReaction from './Routes/AcReaction.js';
import Historic from './Routes/Historic.js';
import DlApk from './Routes/DlApk.js';
import Axios from 'axios';

const checkTriggers = () => {
  Axios.get("http://"+ glob.SERVER_IP + ":8080/services/checkTriggers").then(res => {
    glob.historic = glob.historic.concat(res.data);
  });
}

setInterval(checkTriggers, glob.delayCheckTrigger * 1000);

export default class App extends React.Component {
  render () {
    return (
      <Router>
        <Switch>
          <Route path="/login" render={routeProps => <FormTabs {...routeProps}/>}/>
          <Route path="/Services">
            <Home content={<Services/>}/>
          </Route>
          <Route path="/AcReaction">
            <Home content={<AcReaction/>}/>
          </Route>
          <Route path="/Trigger">
            <Home content={<Trigger/>}/>
          </Route>
          <Route path="/Historic">
            <Home content={<Historic/>}/>
          </Route>
          <Route path="/client.apk">
            <Home content={<DlApk/>}/>
          </Route>
        <Redirect exact from="/" to="/login"/>
        </Switch>
      </Router>
    );
  }
}