import React from 'react';
import axios from 'axios';
import {SERVER_IP} from '../globals';

GetDeezerFollower = () => {
    axios.get("http://" + SERVER_IP + ":8080/deezer/action/followers")
    .then (res => {
        return res;
    })
}

GetDeezerRecommendation = () => {
    axios.get("http://" + SERVER_IP + ":8080/deezer/action/recommendations")
    .then (res => {
        return res;
    })
}

GithubCommit = () => {
    const inf = {
        Iowner = owner,
        Irepo = repo,
    }
    axios.get("http://" + SERVER_IP + ":8080/github/action/commit", inf)
}