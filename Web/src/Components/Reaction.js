import React from 'react';
import axios from 'axios';
import {SERVER_IP} from '../globals';

TrelloNotifClear = () => {
    axios.post("http://" + SERVER_IP + ":8080/trello/reaction/notif")
}

GithubClone = () => {
    const inf = {
        Iowner = '',
        Irepo = '',
        Ibase = '',
        Ihead = '',
        Icommitmsg = '',
    }
    axios.get("http://" + SERVER_IP + ":8080/github/reaction/clone", inf)
}

MailJetSendMail = () => {
    const inf = {
        Iemailto = '',
        Iname = '',
        Isubject = '',
        Imsg = '',
    }
    axios.post("http://" + SERVER_IP + ":8080/mailjet/reaction/sendmail", inf)
}