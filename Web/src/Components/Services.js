import React from 'react'
import axios from 'axios';
import Switch from '@material-ui/core/Switch';
import {SERVER_IP} from '../globals';
import './Services.css';

axios.defaults.withCredentials = true;

export default class Services extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      services: []
    };
    this.getServicesInfos = this.getServicesInfos.bind(this);
    this.addRmvService = this.addRmvService.bind(this);
  }

  addRmvService = (name) => {
    if (this.state.services.includes(name.toLowerCase()))
      axios.get("http://" + SERVER_IP + ":8080/services/remove?name=" + name.toLowerCase()).then(res => {
        this.getServicesInfos();
      });
    else
      window.location.replace("http://" + SERVER_IP + ":8080/services/add?name=" + name.toLowerCase());
  }

  getServicesInfos = () => {
    return new Promise((resolve, reject) => {
      axios.get("http://" + SERVER_IP + ":8080/services/infos").then(res => {
        let services = [];
        res.data.forEach(elem => {
          services.push(elem.name.toLowerCase());
        });
        if (services.length !== this.state.services.length) {
          this.setState({services: services});
        }
      });
    });
  }

    render() {
      this.getServicesInfos();
        return (
        <div className="services">
          <h1>My services:</h1>
              <div>
                <Switch
                    color="primary"
                    checked={this.state.services.includes("discord".toLowerCase())}
                    onClick={() => this.addRmvService("discord")}
                />
                <span className="names">Discord</span>
              </div>
              <div>
                <Switch
                    color="primary"
                    checked={this.state.services.includes("trello".toLowerCase())}
                    onClick={() => this.addRmvService("trello")}
                />
                <span className="names">Trello</span>
                </div>
              <div>
                <Switch
                    color="primary"
                    checked={this.state.services.includes("deezer".toLowerCase())}
                    onClick={() => this.addRmvService("deezer")}
                />
                <span className="names">Deezer</span>
                </div>
              <div>
                <Switch
                    color="primary"
                    checked={this.state.services.includes("spotify".toLowerCase())}
                    onClick={() => this.addRmvService("spotify")}
                />
                <span className="names">Spotify</span>
                </div>
              <div>
                <Switch
                    color="primary"
                    checked={this.state.services.includes("mailjet".toLowerCase())}
                    onClick={() => this.addRmvService("mailjet")}
                />
                <span className="names">MailJet</span>
                </div>
              <div>
                <Switch
                    color="primary"
                    checked={this.state.services.includes("github".toLowerCase())}
                    onClick={() => this.addRmvService("github")}
                />
                <span className="names">Github</span>
                </div>
              <div>
                <Switch
                    color="primary"
                    checked={this.state.services.includes("openweather".toLowerCase())}
                    onClick={() => this.addRmvService("openweather")}
                />
                <span className="names">OpenWeather</span>
                </div>
            </div>
    );
  }
}