import React from "react";
import '../../node_modules/react-grid-layout/css/styles.css';
import '../../node_modules/react-resizable/css/styles.css';
import { WidthProvider, Responsive } from "react-grid-layout";
import _ from "lodash";


import Modal from 'react-awesome-modal';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Select from '@material-ui/core/Select';
import Slider from '@material-ui/core/Slider';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import TimerRounded from '@material-ui/icons/TimerRounded';


const ResponsiveReactGridLayout = WidthProvider(Responsive);

/**
 * This layout demonstrates how to use a grid with a dynamic number of elements.
 */
export default class AddRemoveLayout extends React.PureComponent {
  static defaultProps = {
    className: "layout",
    cols: { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 },
    rowHeight: 100
  };

  constructor(props) {
    super(props);

    this.state = {
      items: [0].map(function(i, key, list) {
        return {
          i: i.toString(),
          x: i * 2,
          y: 0,
          w: 2,
          h: 2,
          add: i === (list.length - 1).toString()
        };
      }),
      newCounter: 1,
      modalVisible: false,
      service: 'none',
      refreshValue: 1
    };

    this.onAddItem = this.onAddItem.bind(this);
    this.onRemoveItem = this.onRemoveItem.bind(this);
    this.onLayoutChange = this.onLayoutChange.bind(this);
    this.onBreakpointChange = this.onBreakpointChange.bind(this);
  }

  createElement(el) {

    const i = el.add ? "+" : el.i;
      return (
        <div key={i} data-grid={el} style={{margin: 10, padding: 10}}>
          <Card  style={{boxShadow: `2px 2px 2px 2px 2px #bfbfbf`, minWidth: 275}}>
            <CardContent>
              <Typography style={{fontsize: 14}} color="primary" gutterBottom>
                Word of the Day
              </Typography>
              <Typography variant="h5" component="h2">
                be
                nev
                lent
              </Typography>
              <Typography style={{marginBottom: 12}} color="primary">
                adjective
              </Typography>
              <Typography variant="body2" component="p">
                well meaning and kindly.
                <br />
                {'"a benevolent smile"'}
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" style={{color: 'red'}} onClick={(i) => {console.log(el.i); this.onRemoveItem(el.i);}}>Delete Widget</Button>
            </CardActions>
          </Card>
        </div>
      );
  }

  onAddItem() {
    if (this.state.newCounter < 12) {
        this.setState({
          items: this.state.items.concat({
            i: "n" + this.state.newCounter,
            x: (this.state.items.length * 2) % (this.state.cols || 12),
            y: Infinity, // puts it at the bottom
            w: 2,
            h: 2
          }),
          newCounter: this.state.newCounter + 1
      });
    }
  }

  onBreakpointChange(breakpoint, cols) {
    this.setState({
      breakpoint: breakpoint,
      cols: cols
    });
  }

  onLayoutChange(layout) {
    this.setState({ layout: layout });
  }

  onRemoveItem(i) {
    this.setState({ items: _.reject(this.state.items, { i: i }) });
  }

  openModal() {
    this.setState({modalVisible: true});
  }

  closeModal() {
    this.setState({modalVisible: false});
  }

  handleSelectService = event => {
    console.log(event.target.value)
    this.setState({service: event.target.value});
  }

  handleSliderChange = (event, newValue) => {
    this.setState({refreshValue: newValue});
  };

  handleInputChange = event => {
    this.setState({refreshValue: event.target.value === '' ? '' : Number(event.target.value)});
  };

  handleBlur = () => {
    console.log(this.state.refreshValue);
    if (this.state.refreshValue < 1) {
      this.setState({refreshValue: 1});
    } else if (this.state.refreshValue > 60) {
      this.setState({refreshValue: 60});
    }
  };

  render() {
    return (
      <div>
        <button onClick={() => this.openModal()}>Open modal</button>
        <Modal visible={this.state.modalVisible} width="400" height="300" effect="fadeInDown" onClickAway={() => this.closeModal()}>
          <div>
            <h1 style={{marginLeft: 2}}>Create your Widget</h1>
              <div>
                <span style={{marginRight: 4, marginLeft: 4}}> Choose your service : </span>
              <Select
                native
                value={this.state.service}
                onChange={this.handleSelectService}
                inputProps={{
                  name: 'age',
                  id: 'filled-age-native-simple',
                }}
              >
                <option value="" />
                <option value={"Weather"}>Weather</option>
                <option value={"Facebook"}>Facebook</option>
              </Select>
              </div>
              <div style={{marginTop: 4, marginLeft: 4, width: 250}}>
                <Typography id="input-slider" gutterBottom>
                  Refresh time (s)
                </Typography>
                <Grid container spacing={2} alignItems="center">
                  <Grid item>
                    <TimerRounded />
                  </Grid>
                  <Grid item xs>
                    <Slider
                      value={typeof this.state.refreshValue === 'number' ? this.state.refreshValue : 0}
                      onChange={this.handleSliderChange}
                      aria-labelledby="input-slider"
                      max={60}
                      min={1}
                    />
                  </Grid>
                  <Grid item>
                    <Input
                      style={{width: 42}}
                      value={this.state.refreshValue}
                      margin="dense"
                      onChange={this.handleInputChange}
                      onBlur={this.handleBlur}
                      inputProps={{
                        step: 5,
                        min: 1,
                        max: 60,
                        type: 'number',
                        'aria-labelledby': 'input-slider',
                      }}
                    />
                  </Grid>
                </Grid>
              </div>
          </div>
        </Modal>
        <button onClick={this.onAddItem}>Add Item</button>
        <ResponsiveReactGridLayout
          onLayoutChange={this.onLayoutChange}
          onBreakpointChange={this.onBreakpointChange}
          {...this.props}
        >
          {_.map(this.state.items, el => this.createElement(el))}
        </ResponsiveReactGridLayout>
      </div>
    );
  }
}