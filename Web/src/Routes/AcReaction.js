import React from 'react';
import axios from 'axios';
import {SERVER_IP} from '../globals';
import {Select, InputLabel, MenuItem, TextField, Button} from '@material-ui/core';

export default function AcReaction() {
    const [actionValue, setActionValue] = React.useState("/services/github/action/commit");
    const [reactionValue, setReactionValue] = React.useState("/services/discord/reaction/message");
    const [actionSetup, setActionSetup] = React.useState(<ConfGithubAction/>);
    const [reactionSetup, setReactionSetup] = React.useState(<ConfDiscordReactionMessage/>);
    const [actionParams, setActionParams] = React.useState({});
    const [reactionParams, setReactionParams] = React.useState({});

    function ConfGithubAction() {
        setActionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleActionParamsChange}>
                <TextField label="Owner" id="owner"/>
                <TextField label="Repository" id="repo"/>
            </form>
        );
    }

    function ConfDiscordActionMemberChannel() {
        setActionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleActionParamsChange}>
                <TextField label="Guild" id="guild"/>
            </form>
        );
    }

    function ConfDiscordActionMessage() {
        setActionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleActionParamsChange}>
                <TextField label="Guild" id="guild"/>
                <TextField label="Channel" id="channel"/>
            </form>
        );
    }

    function ConfOpenWeatherActionTemp() {
        setActionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleActionParamsChange}>
                <TextField label="City" id="city"/>
            </form>
        );
    }

    function ConfOpenWeatherActionForecast() {
        setActionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleActionParamsChange}>
                <TextField label="City" id="city"/>
            </form>
        );
    }

    function ConfActionNothing() {
        setActionParams({});
        return (
            <br/>
        );
    }

    function ConfDiscordReactionMessage() {
        setReactionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleReactionParamsChange}>
                <TextField label="Guild" id="guild"/>
                <TextField label="Channel" id="channel"/>
                <TextField label="Message" id="message"/>
            </form>
        );
    }

    function ConfDiscordReactionRole() {
        setReactionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleReactionParamsChange}>
                <TextField label="User" id="user"/>
                <TextField label="Guild" id="guild"/>
                <TextField label="Role" id="role"/>
            </form>
        );
    }

    function ConfGithubReactionMerge() {
        setReactionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleReactionParamsChange}>
                <TextField label="Owner" id="owner"/>
                <TextField label="Repo" id="repo"/>
                <TextField label="Base" id="base"/>
                <TextField label="Head" id="head"/>
                <TextField label="Commit message" id="commit_message"/>
            </form>
        );
    }

    function ConfMailjetReactionSendmail() {
        setReactionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleReactionParamsChange}>
                <TextField label="Email to" id="emailTo"/>
                <TextField label="Name" id="name"/>
                <TextField label="Subject" id="subject"/>
                <TextField label="Message" id="message"/>
            </form>
        );
    }

    function ConfSpotifyReactionSkip() {
        setReactionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleReactionParamsChange}>
                <TextField label="Skip" id="skip"/>
            </form>
        );
    }

    function ConfSpotifyReactionState() {
        setReactionParams({});
        return (
            <form noValidate autoComplete="off" onChange={handleReactionParamsChange}>
                <TextField label="State" id="state"/>
            </form>
        );
    }

    function ConfReactionNothing() {
        setReactionParams({});
        return (
            <br/>
        );
    }

    const handleActionParamsChange = event => {
        actionParams[event.target.id] = event.target.value;
        setActionParams(actionParams);
    }

    const handleReactionParamsChange = event => {
        reactionParams[event.target.id] = event.target.value;
        setReactionParams(reactionParams);
    }

    const handleActionChange = event => {
        setActionValue(event.target.value);
        if (event.target.value === "/services/deezer/action/followers" || event.target.value === "/services/deezer/action/recommendations" ||
            event.target.value === "/services/spotify/action/currentTrack" || event.target.value === "/services/spotify/action/topArtist")
            setActionSetup(<ConfActionNothing/>);
        else if (event.target.value === "/services/github/action/commit" || event.target.value === "/services/github/action/clone")
            setActionSetup(<ConfGithubAction/>);
        else if (event.target.value === "/services/discord/action/members" || event.target.value === "/services/discord/action/channels")
            setActionSetup(<ConfDiscordActionMemberChannel/>);
        else if (event.target.value === "/services/discord/action/channels/message")
            setActionSetup(<ConfDiscordActionMessage/>);
        else if (event.target.value === "/services/openweather/action/temp")
            setActionSetup(<ConfOpenWeatherActionTemp/>);
        else if (event.target.value === "/services/openweather/action/weatherforecast")
            setActionSetup(<ConfOpenWeatherActionForecast/>);
    }

    const handleReactionChange = event => {
        setReactionValue(event.target.value);
        if (event.target.value === "/services/trello/reaction/notif")
            setReactionSetup(<ConfReactionNothing/>);
        else if (event.target.value === "/services/discord/reaction/message")
            setReactionSetup(<ConfDiscordReactionMessage/>);
        else if (event.target.value === "/services/discord/reaction/role")
            setReactionSetup(<ConfDiscordReactionRole/>);
        else if (event.target.value === "/services/github/reaction/merge")
            setReactionSetup(<ConfGithubReactionMerge/>);
        else if (event.target.value === "/services/mailjet/reaction/sendmail")
            setReactionSetup(<ConfMailjetReactionSendmail/>);
        else if (event.target.value === "/services/spotify/reaction/playback/skip")
            setReactionSetup(<ConfSpotifyReactionSkip/>);
        else if (event.target.value === "/services/spotify/reaction/playback/state")
            setReactionSetup(<ConfSpotifyReactionState/>);
    }

    const handleAddTrigger = () => {
        const jsonTrigger = {
            "trigger": {
                "action": {
                    "route": actionValue,
                    "params": actionParams
                },
                "reaction": {
                    "route": reactionValue,
                    "params": reactionParams
                },
                "data": null
            }
        };
        axios.post(
            "http://" + SERVER_IP + ":8080/services/addTrigger",
            jsonTrigger,
            {headers: {"Content-Type": "application/json"}}
        ).then(res => {
            window.location.replace("http://" + SERVER_IP + ":8081/AcReaction");
        });
    }

    return (
        <div className="AcReaction">
            <InputLabel id="Action">Choose an action</InputLabel>
            <Select labelId="Action" id="select" value={actionValue} onChange={handleActionChange}>
                <MenuItem value="/services/github/action/commit">new github commit</MenuItem>
                <MenuItem value="/services/github/action/clone">new github clone</MenuItem>
                <MenuItem value="/services/deezer/action/followers">new deezer follower</MenuItem>
                <MenuItem value="/services/deezer/action/recommendations">new deezer recommendations</MenuItem>
                <MenuItem value="/services/discord/action/members">new discord member</MenuItem>
                <MenuItem value="/services/discord/action/channels">new discord channel</MenuItem>
                <MenuItem value="/services/discord/action/channels/message">new discord message</MenuItem>
                <MenuItem value="/services/spotify/action/currentTrack">new spotify current track</MenuItem>
                <MenuItem value="/services/spotify/action/topArtist">new spotify top artist</MenuItem>
                <MenuItem value="/services/openweather/action/temp">new openweather temperature</MenuItem>
                <MenuItem value="/services/openweather/action/weatherforecast">new openweather weather forecast</MenuItem>
            </Select>
            <br/>
            <br/>
            {actionSetup}
            <br/>
            <br/>
            <InputLabel id="Reaction">Choose a reaction</InputLabel>
            <Select labelId="Reaction" id="select" value={reactionValue} onChange={handleReactionChange}>
                <MenuItem value="/services/discord/reaction/message">post new discord message</MenuItem>
                <MenuItem value="/services/discord/reaction/role">add a role to a discord user</MenuItem>
                <MenuItem value="/services/github/reaction/merge">merge a github repo</MenuItem>
                <MenuItem value="/services/mailjet/reaction/sendmail">send a mail</MenuItem>
                <MenuItem value="/services/trello/reaction/notif">clear all trello notifications</MenuItem>
                <MenuItem value="/services/spotify/reaction/playback/skip">skip current track on spotify</MenuItem>
                <MenuItem value="/services/spotify/reaction/playback/state">change the player state on spotify</MenuItem>
            </Select>
            <br/>
            <br/>
            {reactionSetup}
            <br/>
            <br/>
            <Button variant="contained" onClick={handleAddTrigger}>Add Trigger</Button>
        </div>
    );
}
