import React from 'react';
import {Button} from '@material-ui/core';

export default function DlApk() {

    return (
        <div className="DlApk">
            <a href="../../apk/client.apk" download><Button variant="contained">Download APK</Button></a>
        </div>
    );
}
