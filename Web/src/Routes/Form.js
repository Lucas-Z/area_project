import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import {SERVER_IP} from '../globals';

export default class Form extends React.Component {

  constructor() {
    super();

    this.state = {
      showMenu: false,
      showMenu2: false,
    };

    this.showMenu = this.showMenu.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
    this.showMenu2 = this.showMenu2.bind(this);
    this.closeMenu2 = this.closeMenu2.bind(this);
  }

  showMenu(event) {
    event.preventDefault();

    this.setState({ showMenu: true }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

  showMenu2(event) {
    event.preventDefault();

    this.setState({ showMenu2: true }, () => {
      document.addEventListener('click', this.closeMenu2);
    });
  }

  closeMenu(event) {

    if (!this.dropdownMenu.contains(event.target)) {

      this.setState({ showMenu: false }, () => {
        document.removeEventListener('click', this.closeMenu);
      });

    }
  }

  closeMenu2(event) {

    if (!this.dropdownMenu2.contains(event.target)) {

      this.setState({ showMenu2: false }, () => {
        document.removeEventListener('click', this.closeMenu2);
      });

    }
  }
    state = {
        parameter1: false,
        parameter2: false,
        parameter3: false,
        parameter4: false,
        selectError: false,
    }


    handleParameters1 = (e) => this.setState({
		parameter1: e.target.value
	})
    handleParameters2 = (e) => this.setState({
		parameter2: e.target.value
    })
    handleParameters3 = (e) => this.setState({
		parameter3: e.target.value
    })
    handleParameters4 = (e) => this.setState({
		parameter4: e.target.value
    })

     render() {
        const enterParam = () => {
            const submitParam = {
                parameter1: this.state.parameter1,
                parameter2: this.state.parameter2,
                parameter3: this.state.parameter3,
                parameter4: this.state.parameter4,
            }
            axios.post("http://" + SERVER_IP + ":8080/form/sub", submitParam)
            .then (res => {
                console.log(res);
                if (res.data !== "Successfully submitted parameters")
                    this.setState({selectError: false});
                   else {
                       this.setState({selectError: true})
                       this.props.history.push('/form');
                   }
           })
       }

        return (
          <div>
            <button onClick={this.showMenu}>
              Show services
            </button>
            <span>
          {
            this.state.showMenu
              ? (
                <div
                  className="menu"
                  ref={(element) => {
                    this.dropdownMenu = element;
                  }}
                >
                  <button> service 1 </button>
                  <button> service 2 </button>
                  <button> service 3 </button>
                </div>
              )
              : (
                null
              )
          }
          <li></li>
            <button onClick={this.showMenu2}>
              Show actions
            </button>
          {
            this.state.showMenu2
              ? (
                <React.Fragment>
                <div
                  className="menu"
                  ref={(element) => {
                    this.dropdownMenu2 = element;
                  }}
                >
                  <button> action 1 </button>
                  <button> action 2 </button>
                  <button> action 3 </button>
                </div>
                </React.Fragment>
                )
              : (
                null
              )
          }
          <li></li>

          </span>
          <TextField
                        error={this.state.selectError}
                        value={this.state.parameter1}
                        onChange= {this.handleParameters1}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="serv"
                        label="first parameter"
                        autoComplete="current action"
                    />
                    <TextField
                        error={this.state.selectError}
                        value={this.state.parameter2}
                        onChange= {this.handleParameters2}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        label="second parameter"
                        id="action"
                        type="action"
                        autoComplete="current action"
                    />
                    <TextField
                        error={this.state.selectError}
                        value={this.state.parameter3}
                        onChange= {this.handleParameters3}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="serv"
                        label="third parameter"
                        autoComplete="current action"
                    />
                    <TextField
                        error={this.state.selectError}
                        value={this.state.parameter4}
                        onChange= {this.handleParameters4}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="serv"
                        label="fourth parameter"
                        autoComplete="current action"
                    />

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        onClick={enterParam}
                    >
                        ENTER
                    </Button>
                </div>
          
        );
    }
}
