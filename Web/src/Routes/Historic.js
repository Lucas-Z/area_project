import React from 'react';
import {historic} from '../globals';

function Historic() {

    return (<div style={{textAlign: "center", fontSize: 18}}>
              Historic
                <ul>
                  {historic.map((elem, idx) => (
                    <li style={{textDecoration: 'none'}} key={idx}>{elem}</li>
                  ))}
                </ul>
    </div>)
};

export default Historic