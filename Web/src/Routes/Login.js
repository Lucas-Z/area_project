import React from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {SERVER_IP} from '../globals';

export default class Login extends React.Component {

    state = {
        email: '',
        pass: '',
        loginError: false,
    }

    handlePass = (e) => this.setState({
		pass: e.target.value
	})

    handleEmail = (e) => this.setState({
		email: e.target.value
	})

     render() {

         const loginInfo = () => {
             const logInf = {
                 username: this.state.email,
                 password: this.state.pass,
             }
             axios.post("http://" + SERVER_IP + ":8080/auth/login", logInf)
             .then (res => {
                 console.log(res);
                 if (res.status !== 200)
                        this.setState({loginError: true});
                    else {
                        this.setState({loginError: false})
                        this.props.history.push('/Services');
                    }
            })
        }

        const responseGoogle = () => {
            window.location.replace("http://" + SERVER_IP + ":8080/auth/google");
        }

        const responseGithub = () => {
            window.location.replace("http://" + SERVER_IP + ":8080/auth/github");
        }

        return (
            <div>
                <div>
                    <TextField
                        error={this.state.loginError}
                        value={this.state.email}
                        onChange= {this.handleEmail}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        autoComplete="email"
                    />
                    <TextField
                        error={this.state.loginError}
                        value={this.state.pass}
                        onChange= {this.handlePass}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        label="Password"
                        id="password"
                        type="password"
                        autoComplete="current-password"
                    />
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        onClick={loginInfo}
                    >
                        Log In
                    </Button>
                </div>
                <div style={{display: 'flex', paddingLeft: 5, marginTop: 20, paddingBottom: 10}}>
                    <span style={{color: "grey"}}>———————————————</span>
                    <span style={{color: "grey", marginLeft: 2, marginRight: 2}}>OR</span>
                    <span style={{color: "grey"}}>———————————————</span>
                </div>
                <div style={{display: 'flex', marginTop: 15}}>
                    <div style={{flex: 1, alignItems: 'center'}}>
                        <Button onClick={responseGoogle}>
                            LOGIN WITH GOOGLE
                        </Button>
                    </div>
                    <div>
                        <Button onClick={responseGithub}>
                            LOGIN WITH GITHUB
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}