import React from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {SERVER_IP} from '../globals';

export default class Signin extends React.Component {
    state = {
        name: '',
        email: '',
        pass: '',
        confirmp: '',
        signinError: false
    }

    handleName = (evt) => this.setState({
        name: evt.target.value
    })

    handlePass = (evt) => this.setState({
		pass: evt.target.value
    })

    handleConfirmP = (evt) => this.setState({
        confirmp: evt.target.value
    })

    handleEmail = (evt) => this.setState({
		email: evt.target.value
	})
    render() {

        const signInInfo = () => {
            if (this.state.email === '' || this.state.pass === '' ||
                this.state.confirmp === '' || this.state.confirmp !== this.state.pass)
                this.setState({signinError: true});
            else {
                const signInf = {
                    username: this.state.email,
                    password: this.state.pass,
                }
                console.log(signInf);
                axios.post("http://" + SERVER_IP + ":8080/auth/signup", signInf, { withCredentials: true })
                .then(res => {
                    console.log(res.status);
                    if (res.status !== 201)
                        this.setState({loginError: true});
                    else {
                        this.setState({loginError: false})
                        this.props.history.push('/home');
                    }
                })
            }
        }

        const responseGoogle = () => {
            window.location.replace("http://" + SERVER_IP + ":8080/auth/google");
        }

        const responseGithub = () => {
            window.location.replace("http://" + SERVER_IP + ":8080/auth/github");
        }

        return (
            <div>
                <div>
                    <TextField
                        error={this.state.signinError}
                        value={this.state.email}
                        onChange={this.handleEmail}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        autoFocus
                    />
                    <TextField
                        error={this.state.signinError}
                        value={this.state.pass}
                        onChange={this.handlePass}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        autoFocus
                    />
                    <TextField
                        error={this.state.signinError}
                        value={this.state.confirmp}
                        onChange={this.handleConfirmP}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="Confirm password"
                        label="Confirm your Password"
                        type="password"
                        id="Confirm-password"
                        autoComplete="Current-password"
                    />
                    <Button
                        style={{marginTop: 10}}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        onClick={signInInfo}
                    >
                        Sign up
                    </Button>
                </div>
                <div style={{display: 'flex', paddingLeft: 5, marginTop: 20, paddingBottom: 10}}>
                    <span style={{color: "grey"}}>———————————————</span>
                    <span style={{color: "grey", marginLeft: 2, marginRight: 2}}>OR</span>
                    <span style={{color: "grey"}}>———————————————</span>
                </div>
                <div style={{display: 'flex', marginTop: 15}}>
                    <div style={{flex: 1, alignItems: 'center'}}>
                        <Button onClick={responseGoogle}>
                            LOGIN WITH GOOGLE
                        </Button>
                    </div>
                    <div>
                        <Button onClick={responseGithub}>
                            LOGIN WITH GITHUB
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}