import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {SERVER_IP} from '../globals';

const Trigger = () => {

    const [triggers, setTrigger] = useState([]);

    useEffect(() => {
        async function fetchData() {
        await axios.get("http://"+ SERVER_IP +":8080/services/getTriggersDesc")
        .then (res => {
            res.data.forEach((elem, idx) => {
                console.log(elem);
                console.log(idx);
                setTrigger(triggers => [
                    ...triggers,
                    {
                    id: idx,
                    value: elem[0] + " --> " + elem[1]
                    }
                ]);
            });
        })
    }fetchData();
    }, [],
    );

    console.log("triggers : ")
    console.log(triggers)

    return(
        <div>
            Your triggers
            <ul>
            {triggers.map(trigger => (
            <li key={trigger.id}>{trigger.value}</li>
            ))}
        </ul>
      </div>
    )
};

export default Trigger